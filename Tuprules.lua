ROOT = tup.getcwd()
BOOTSTRAP = ROOT.."/bootstrap"

-- Central shelving list
shelved = {}
local shelved_list
if pcall(function() shelved_list = assert(io.open('shelved_features', 'r')) end) then
   for feature in shelved_list:lines() do
	  if feature ~= '' then
		 shelved[feature] = true
	  end
   end
   shelved_list:close()
end
