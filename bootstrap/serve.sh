#!/bin/bash

# Serve the web site from inside a running build container.

set -e

open 'http://localhost:8000'
docker exec wsbuild live-server --entry-file=index.html --port=8080 site

exit 0
