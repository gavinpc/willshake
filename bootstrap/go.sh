#!/bin/bash

# Start the build container with most source folder (and site) mounted.
#
# ASSUMES the build image already exists and is tagged `willshake_build`.
#
# ASSUMES the parent directory is the project root
#
# ASSUMES a specific location of the “mirror” directory

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
ws_dir="$this_script_dir/.."
mirror_dir="$HOME/ws/mirror"

port="${1:-${PORT:-8000}}"

# `--privileged` is needed to run Tup (because of FUSE)
#
# The default entrypoint is the build monitor, but I still find I need the
# container open for working out the kinks.  Also the console output looks
# better when not forwarded to the host.
docker run \
       --privileged \
       --name wsbuild \
       -p "$port:8080" \
       -p "35729" \
       -v "$mirror_dir:/root/ws/mirror/" \
       -v "$ws_dir/documents:/ws/documents" \
       -v "$ws_dir/text:/ws/text" \
       -it --entrypoint bash \
       willshake_build

# Tup gives an error like this if you mount certain directories, including
# database (an input) and site (an output).
#
# tup/tmp/backup-24224: Invalid cross-device link
# tup error: Unable to move output file './database/images.xml' to temporary location '.tup/tmp/backup-24224'
#
#       -v "$ws_dir/database:/ws/database" 
#       -v "$ws_dir/site:/ws/site" 

exit 0
