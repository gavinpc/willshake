#!/bin/bash

# Stop a build container started by `start.sh`.

set -e

docker rm wsbuild

exit 0
