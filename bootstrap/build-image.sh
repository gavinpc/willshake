#!/bin/sh

# Build the Dockerfile for the builder.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
context_dir="${1:-$this_script_dir/..}"

docker build \
       --tag willshake_build \
       --file "$this_script_dir"/Dockerfile \
       "$context_dir"

exit 0
