-- ASSUMES the bootstrapper is in a first-level directory.  I don't know how to
-- dynamically get a path back to the root from here.
local bootstrapper_to_root = '..'
local root_to_processing = tup.getrelativedir(ROOT)
local processing_dir = bootstrapper_to_root..'/'..root_to_processing

-- Destructively collect a list of graph nodes sorted by dependency.  Not
-- worried about falsy keys or values.
function sort_node(g, k, sorted, stack)
   stack[k] = 1				-- for cycle detection
   -- Does anything link to this?  If so, deal with them first.
   for _, o in next, g[k] do
	  if not stack[o] and g[o] then
		 sort_node(g, o, sorted, stack)
	  end
   end
   table.insert(sorted, k)
   g[k] = nil
end
function sort_keys(g)
   local sorted = {}
   -- How to traverse an array while killing it
   local k = next(g)
   while k do
	  sort_node(g, k, sorted, {})
	  k = next(g)
   end
   return sorted
end

-- Merge two tables by key assuming values are also tables.
function merge_dictionaries(left, right)
   for key, right_table in next, right do
	  if not left[key] then left[key] = {} end
	  tup.append_table(left[key], right_table)
   end
end

function tokenize(args)
   local t = args.into or {}
   for x in args[1]:gmatch(args.on or '%S+') do table.insert(t, x) end
   return t
end

-- Support input and output lists, including "extras"
function parse_files(expression, extra_field)
   local mains, more = expression:match'(.*) | (.*)'
   local items = tokenize{mains or expression}
   if more then items[extra_field] = tokenize{more} end
   return items
end

function get_rules(base_dir, filename)
   local tangles = {}
   local builds = {}
   local services = {}
   local supers = {}
   local used_services = {}
   
   local tangle_targets = {}
   
   function process(line)
	  
	  -- These directives are always on #+BEGIN_SRC lines.  It's
	  -- case-insensitive in org-mode, so we don't check for the whole thing.
	  -- This is just enough to avoid false positives from other text.
	  if line:match'^#' then
		 
		 -- Document metadata
		 local property, value = line:match'^#%+PROPERTY: (%S+) (%S+)'
		 
		 -- Support shelved documents
		 if property == 'status' and value == 'shelved' then
			return false
		 end
		 
		 -- Support opt-in services
		 if property == 'uses_service' then
			used_services[value] = true
		 end
		 
		 -- Code blocks.  Note that a tangle file may be listed more than once.
		 local target = line:match':tangle (%S+)'
		 if target then
			-- Um...
			target = base_dir..'/tangled/'..target
			
			-- All tangles go into a group.  You can set the name.  If the file
			-- uses multiple blocks, the first block is used.
			if not tangle_targets[target] then
			   tangle_targets[target] = line:match':group (%S+)' or 'all'
			end
		 end
	  end
	  
	  -- Collect build rules in Tup syntax
	  local super, ins, command, outs = line:match'^(.*): (.*) |> (.*) |> (.*)$'
	  if outs then
		 -- Support services
		 local service = super:match'^service (%S+)'
		 
		 -- Support "foreach" rules
		 local is_foreach = ins:match'^foreach '
		 if is_foreach then
			ins = ins:sub(string.len('foreach ') + 1)
		 end
		 
		 local output_dir = outs:match'%S+/'
		 
		 local rule = {
			is_foreach = is_foreach,
			ins = ins, inputs = parse_files(ins, 'extra_inputs'),
			command = command:gsub('%%p', root_to_processing),
			outputs = parse_files(outs, 'extra_outputs'),
			output_dir = output_dir }
		 
		 -- All build outputs go into a group.  If one is not specified
		 -- explicitly in the rule, a group called `<all>' is used, in the
		 -- directory of the first output.
		 if not outs:match'>$' then rule.group = output_dir..'<all>' end
		 if super:match'^super ' then
			table.insert(supers, rule)
		 elseif service then
			services[service] = services[service] or {}
			table.insert(services[service], rule)
		 else
			table.insert(builds, rule)
		 end
	  end
	  
	  return true
   end
   
   -- The more concise io.lines *will not work* here.  Tup will not notice files
   -- opened by io.lines, and thus won't reprocess rules when they change.
   local line
   local file = assert(io.open(processing_dir..'/'..filename, 'r'))
   for raw_line in file:lines() do
	  -- Support continued lines.  Note that `raw_line' evidently includes the
	  -- newline.
	  if line and line:match'\\$' then
		 line = line:sub(0, -2)..raw_line
	  else
		 if not process(line or raw_line) then return {} end
		 line = raw_line
	  end
   end
   file:close()
   
   -- Tangle
   if tangle_targets then
	  
	  local outputs = {}
	  for tangled in next, tangle_targets do table.insert(outputs, tangled) end
	  table.insert(
		 tangles, {
			inputs = {filename},
			command = '^o tangle %B^ '..BOOTSTRAP..'/tangle --assign ROOT='..ROOT..'/tangled "%f"',
			outputs = outputs})
	  
	  -- Now link the "actual" location to the "actual" file.
	  for tangled, group in next, tangle_targets do
		 local real_target = tangled:gsub('/tangled/', '/')
		 
		 -- Support grouping of tangles
		 local folder = real_target:match'.*/' or ''
		 table.insert(
			tangles, {
			   inputs = {tangled},
			   command = '^ link tangle %f^ ln --symbolic --relative "%f" "%o"',
			   outputs = {real_target},
			   group = folder..'<'..group..'>'})
	  end
	  
   end
   
   return {
	  tangles = tangles,
	  builds = builds,
	  services = services,
	  supers = supers,
	  used_services = used_services,
   }
end

function add_build_rules(rules)
   for _, rule in next, rules do
	  local add_rule = tup.rule
	  if rule.is_foreach then add_rule = tup.foreach_rule end
	  if rule.group then
		 local outs = rule.outputs
		 if not outs.extra_outputs then outs.extra_outputs = {} end
		 table.insert(outs.extra_outputs, rule.group)
	  end
	  add_rule(rule.inputs, rule.command, rule.outputs)
   end
end

local documents

-- Is this directory shelved?
local skip = shelved and shelved[root_to_processing]

-- If there are no documents in the directory, `glob` will throw an error.
if not skip and pcall(function() documents = tup.glob('*.org') end) then
   
   local services = {} -- opt-in rules that may be used by descendant subsystems
   local tangles = {}		-- files extracted from documents
   local builds = {}		-- explicit build rules from documents
   local used_services = {}
   
   for _, doc in next, documents do
	  -- Is this document (centrally) shelved?
	  if not shelved[doc] then
		 local rules = get_rules(ROOT, doc, graph, used_services)
		 merge_dictionaries(services, rules.services or {})
		 tup.append_table(builds, rules.supers or {})
		 tup.append_table(builds, rules.builds or {})
		 tup.append_table(tangles, rules.tangles or {})
		 for _ in next, rules.used_services or {} do used_services[_] = true end
	  end
   end
   
   -- Get rules from supersystems
   local up = ''
   for __ in processing_dir:gmatch('/') do
	  up = up..'../'
	  if pcall(function() documents = tup.glob(up..'*.org') end) then
		 for _, doc in next, documents do
			-- Is the *ancestor* document shelved?  Note that ancestor documents
			-- can only be shelved by filename, which is assumed unique.
			if not shelved[tup.file(doc)] then
			   local super_rules = get_rules(ROOT, doc, {}, {})
			   tup.append_table(builds, super_rules.supers or {})
			   merge_dictionaries(services, super_rules.services or {})
			end
		 end
	  end
   end

   add_build_rules(tangles)
   
   -- Merge service rules into build rules.
   for service, rules in next, services do
	  if used_services[service] then
		 tup.append_table(builds, rules)
	  end
   end
   
   -- Reorder build rules based on dependencies.  This is done using a graph of
   -- the dependencies among input and output directories.
   local graph = {}
   local by_dir = {}
   for _, rule in next, builds do
	  local output_dir = rule.output_dir
	  if not by_dir[output_dir] then by_dir[output_dir] = {} end
	  table.insert(by_dir[output_dir], rule)
	  if not graph[output_dir] then graph[output_dir] = {} end
	  graph[output_dir] = tokenize{rule.ins, on='%S+/', into=graph[output_dir]}
   end
   for _, dir in next, sort_keys(graph) do
	  add_build_rules(by_dir[dir])
   end

end
