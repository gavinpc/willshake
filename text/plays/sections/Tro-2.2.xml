<section key="2.2">
	<location group="Troy">Troy. A room in Priam’s palace.</location>
	<sd a="Enter_Priam">
		Enter <enter role="Priam">Priam</enter>,
		<enter role="Hector">Hector</enter>,
		<enter role="Troilus">Troilus</enter>,
		<enter role="Paris">Paris</enter>,
		and <enter role="Helenus">Helenus</enter>
	</sd>
	<speech>
		<speaker role="Priam">Priam</speaker>
		<line a="After_so">After so many hours, lives, speeches spent,</line>
		<line a="Thus_once">Thus once again says Nestor from the Greeks:</line>
		<line a="Deliver_Helen">“Deliver Helen, and all damage else—</line>
		<line a="As_honour">As honour, loss of time, travail, expense,</line>
		<line a="Wounds_friends">Wounds, friends, and what else dear that is consumed</line>
		<line a="In_hot">In hot digestion of this cormorant war—</line>
		<line a="Shall_be">Shall be struck off.” Hector, what say you to’t?</line>
	</speech>
	<speech>
		<speaker role="Hector">Hector</speaker>
		<line a="Though_no">Though no man lesser fears the Greeks than I</line>
		<line a="As_far">As far as toucheth my particular,</line>
		<line a="Yet_dread">Yet, dread Priam,</line>
		<line a="There_is_no">There is no lady of more softer bowels,</line>
		<line a="More_spongy">More spongy to suck in the sense of fear,</line>
		<line a="More_ready">More ready to cry out “Who knows what follows?”</line>
		<line a="Than_Hector">Than Hector is: the wound of peace is surety,</line>
		<line a="Surety_secure">Surety secure; but modest doubt is call’d</line>
		<line a="The_beacon">The beacon of the wise, the tent that searches</line>
		<line a="To_the">To the bottom of the worst. Let Helen go:</line>
		<line a="Since_the">Since the first sword was drawn about this question,</line>
		<line a="Every_tithe">Every tithe soul, ‘mongst many thousand dismes,</line>
		<line a="Hath_been">Hath been as dear as Helen; I mean, of ours:</line>
		<line a="If_we">If we have lost so many tenths of ours,</line>
		<line a="To_guard">To guard a thing not ours nor worth to us,</line>
		<line a="Had_it">Had it our name, the value of one ten,</line>
		<line a="What_merits">What merit’s in that reason which denies</line>
		<line a="The_yielding">The yielding of her up?</line>
	</speech>
	<speech>
		<speaker role="Troilus">Troilus</speaker>
		<line a="Fie_fie"><catch />Fie, fie, my brother!</line>
		<line a="Weigh_you">Weigh you the worth and honour of a king</line>
		<line a="So_great">So great as our dread father in a scale</line>
		<line a="Of_common">Of common ounces? will you with counters sum</line>
		<line a="The_past">The past proportion of his infinite?</line>
		<line a="And_buckle">And buckle in a waist most fathomless</line>
		<line a="With_spans">With spans and inches so diminutive</line>
		<line a="As_fears">As fears and reasons? fie, for godly shame!</line>
	</speech>
	<speech>
		<speaker role="Helenus">Helenus</speaker>
		<line a="No_marvel">No marvel, though you bite so sharp at reasons,</line>
		<line a="You_are_so">You are so empty of them. Should not our father</line>
		<line a="Bear_the">Bear the great sway of his affairs with reasons,</line>
		<line a="Because_your">Because your speech hath none that tells him so?</line>
	</speech>
	<speech>
		<speaker role="Troilus">Troilus</speaker>
		<line a="You_are_for">You are for dreams and slumbers, brother priest;</line>
		<line a="You_fur">You fur your gloves with reason. Here are your reasons:</line>
		<line a="You_know">You know an enemy intends you harm;</line>
		<line a="You_know_a">You know a sword employ’d is perilous,</line>
		<line a="And_reason">And reason flies the object of all harm:</line>
		<line a="Who_marvels">Who marvels then, when Helenus beholds</line>
		<line a="A_Grecian">A Grecian and his sword, if he do set</line>
		<line a="The_very">The very wings of reason to his heels</line>
		<line a="And_fly">And fly like chidden Mercury from Jove,</line>
		<line a="Or_like">Or like a star disorb’d? Nay, if we talk of reason,</line>
		<line a="Lets_shut">Let’s shut our gates and sleep: manhood and honour</line>
		<line a="Should_have">Should have hare-hearts, would they but fat their thoughts</line>
		<line a="With_this">With this cramm’d reason: reason and respect</line>
		<line a="Make_livers">Make livers pale and lustihood deject.</line>
	</speech>
	<speech>
		<speaker role="Hector">Hector</speaker>
		<line a="Brother_she">Brother, she is not worth what she doth cost</line>
		<line a="The_holding">The holding.</line>
	</speech>
	<speech>
		<speaker role="Troilus">Troilus</speaker>
		<line a="What_is_aught"><catch/>What is aught, but as ‘tis valued?</line>
	</speech>
	<speech>
		<speaker role="Hector">Hector</speaker>
		<line a="But_value">But value dwells not in particular will;</line>
		<line a="It_holds">It holds his estimate and dignity</line>
		<line a="As_well">As well wherein ‘tis precious of itself</line>
		<line a="As_in">As in the prizer: ‘tis mad idolatry</line>
		<line a="To_make">To make the service greater than the god</line>
		<line a="And_the_will">And the will dotes that is attributive</line>
		<line a="To_what_infectiously">To what infectiously itself affects,</line>
		<line a="Without_some">Without some image of the affected merit.</line>
	</speech>
	<speech>
		<speaker role="Troilus">Troilus</speaker>
		<line a="I_take">I take to-day a wife, and my election</line>
		<line a="Is_led">Is led on in the conduct of my will;</line>
		<line a="My_will">My will enkindled by mine eyes and ears,</line>
		<line a="Two_traded">Two traded pilots ‘twixt the dangerous shores</line>
		<line a="Of_will">Of will and judgment: how may I avoid,</line>
		<line a="Although_my">Although my will distaste what it elected,</line>
		<line a="The_wife">The wife I chose? there can be no evasion</line>
		<line a="To_blench">To blench from this and to stand firm by honour:</line>
		<line a="We_turn">We turn not back the silks upon the merchant,</line>
		<line a="When_we">When we have soil’d them, nor the remainder viands</line>
		<line a="We_do">We do not throw in unrespective sieve,</line>
		<line a="Because_we">Because we now are full. It was thought meet</line>
		<line a="Paris_should">Paris should do some vengeance on the Greeks:</line>
		<line a="Your_breath">Your breath of full consent bellied his sails;</line>
		<line a="The_seas">The seas and winds, old wranglers, took a truce</line>
		<line a="And_did">And did him service: he touch’d the ports desired,</line>
		<line a="And_for">And for an old aunt whom the Greeks held captive,</line>
		<line a="He_brought">He brought a Grecian queen, whose youth and freshness</line>
		<line a="Wrinkles_Apollos">Wrinkles Apollo’s, and makes stale the morning.</line>
		<line a="Why_keep">Why keep we her? the Grecians keep our aunt:</line>
		<line a="Is_she">Is she worth keeping? why, she is a pearl,</line>
		<line a="Whose_price">Whose price hath launch’d above a thousand ships,</line>
		<line a="And_turnd">And turn’d crown’d kings to merchants.</line>
		<line a="If_youll">If you’ll avouch ‘twas wisdom Paris went—</line>
		<line a="As_you">As you must needs, for you all cried “Go, go,”—</line>
		<line a="If_youll_confess">If you’ll confess he brought home noble prize—</line>
		<line a="As_you_must">As you must needs, for you all clapp’d your hands</line>
		<line a="And_cried">And cried “Inestimable!”—why do you now</line>
		<line a="The_issue">The issue of your proper wisdoms rate,</line>
		<line a="And_do">And do a deed that fortune never did,</line>
		<line a="Beggar_the">Beggar the estimation which you prized</line>
		<line a="Richer_than">Richer than sea and land? O, theft most base,</line>
		<line a="That_we_have_stoln">That we have stol’n what we do fear to keep!</line>
		<line a="But_thieves">But, thieves, unworthy of a thing so stol’n,</line>
		<line a="That_in">That in their country did them that disgrace,</line>
		<line a="We_fear">We fear to warrant in our native place!</line>
	</speech>
	<speech>
		<speaker role="Cassandra">Cassandra</speaker>
		<line a="Cry_Trojans">
			<sd a="Within">
				Within
			</sd>Cry, Trojans, cry!</line>
	</speech>
	<speech>
		<speaker role="Priam">Priam</speaker>
		<line a="What_noise">What noise? what shriek is this?</line>
	</speech>
	<speech>
		<speaker role="Troilus">Troilus</speaker>
		<line a="Tis_our">‘Tis our mad sister, I do know her voice.</line>
	</speech>
	<speech>
		<speaker role="Cassandra">Cassandra</speaker>
		<line a="Cry_Trojans_2">
			<sd a="Within">
				Within
			</sd>Cry, Trojans!</line>
	</speech>
	<speech>
		<speaker role="Hector">Hector</speaker>
		<line a="It_is_Cassandra">It is Cassandra.</line>
	</speech>
	<sd a="Enter_CASSANDRA">
			Enter CASSANDRA, raving
			
				<enter role="Cassandra"/>
			
		</sd>
	<speech>
		<speaker role="Cassandra">Cassandra</speaker>
		<line a="Cry_Trojans_cry">Cry, Trojans, cry! lend me ten thousand eyes,</line>
		<line a="And_I_will">And I will fill them with prophetic tears.</line>
	</speech>
	<speech>
		<speaker role="Hector">Hector</speaker>
		<line a="Peace_sister">Peace, sister, peace!</line>
	</speech>
	<speech>
		<speaker role="Cassandra">Cassandra</speaker>
		<line a="Virgins_and">Virgins and boys, mid-age and wrinkled eld,</line>
		<line a="Soft_infancy">Soft infancy, that nothing canst but cry,</line>
		<line a="Add_to">Add to my clamours! let us pay betimes</line>
		<line a="A_moiety">A moiety of that mass of moan to come.</line>
		<line a="Cry_Trojans_cry_practise">Cry, Trojans, cry! practise your eyes with tears!</line>
		<line a="Troy_must">Troy must not be, nor goodly Ilion stand;</line>
		<line a="Our_firebrand">Our firebrand brother, Paris, burns us all.</line>
		<line a="Cry_Trojans_cry_a">Cry, Trojans, cry! a Helen and a woe:</line>
		<line a="Cry_cry">Cry, cry! Troy burns, or else let Helen go.</line>
		<sd a="Exit">
			Exit
			
				<exit role="Cassandra"/>
			
		</sd>
	</speech>
	<speech>
		<speaker role="Hector">Hector</speaker>
		<line a="Now_youthful">Now, youthful Troilus, do not these high strains</line>
		<line a="Of_divination">Of divination in our sister work</line>
		<line a="Some_touches">Some touches of remorse? or is your blood</line>
		<line a="So_madly">So madly hot that no discourse of reason,</line>
		<line a="Nor_fear">Nor fear of bad success in a bad cause,</line>
		<line a="Can_qualify">Can qualify the same?</line>
	</speech>
	<speech>
		<speaker role="Troilus">Troilus</speaker>
		<line a="Why_brother"><catch />Why, brother Hector,</line>
		<line a="We_may">We may not think the justness of each act</line>
		<line a="Such_and">Such and no other than event doth form it,</line>
		<line a="Nor_once">Nor once deject the courage of our minds,</line>
		<line a="Because_Cassandras">Because Cassandra’s mad: her brain-sick raptures</line>
		<line a="Cannot_distaste">Cannot distaste the goodness of a quarrel</line>
		<line a="Which_hath">Which hath our several honours all engaged</line>
		<line a="To_make_it">To make it gracious. For my private part,</line>
		<line a="I_am">I am no more touch’d than all Priam’s sons:</line>
		<line a="And_Jove">And Jove forbid there should be done amongst us</line>
		<line a="Such_things">Such things as might offend the weakest spleen</line>
		<line a="To_fight">To fight for and maintain!</line>
	</speech>
	<speech>
		<speaker role="Paris">Paris</speaker>
		<line a="Else_might">Else might the world convince of levity</line>
		<line a="As_well_my">As well my undertakings as your counsels:</line>
		<line a="But_I_attest">But I attest the gods, your full consent</line>
		<line a="Gave_wings">Gave wings to my propension and cut off</line>
		<line a="All_fears">All fears attending on so dire a project.</line>
		<line a="For_what">For what, alas, can these my single arms?</line>
		<line a="What_Propugnation">What Propugnation is in one man’s valour,</line>
		<line a="To_stand">To stand the push and enmity of those</line>
		<line a="This_quarrel">This quarrel would excite? Yet, I protest,</line>
		<line a="Were_I">Were I alone to pass the difficulties</line>
		<line a="And_had">And had as ample power as I have will,</line>
		<line a="Paris_should_neer">Paris should ne’er retract what he hath done,</line>
		<line a="Nor_faint">Nor faint in the pursuit.</line>
	</speech>
	<speech>
		<speaker role="Priam">Priam</speaker>
		<line a="Paris_you"><catch />Paris, you speak</line>
		<line a="Like_one">Like one besotted on your sweet delights:</line>
		<line a="You_have_the">You have the honey still, but these the gall;</line>
		<line a="So_to">So to be valiant is no praise at all.</line>
	</speech>
	<speech>
		<speaker role="Paris">Paris</speaker>
		<line a="Sir_I">Sir, I propose not merely to myself</line>
		<line a="The_pleasures">The pleasures such a beauty brings with it;</line>
		<line a="But_I_would">But I would have the soil of her fair rape</line>
		<line a="Wiped_off">Wiped off, in honourable keeping her.</line>
		<line a="What_treason">What treason were it to the ransack’d queen,</line>
		<line a="Disgrace_to">Disgrace to your great worths and shame to me,</line>
		<line a="Now_to">Now to deliver her possession up</line>
		<line a="On_terms">On terms of base compulsion! Can it be</line>
		<line a="That_so">That so degenerate a strain as this</line>
		<line a="Should_once">Should once set footing in your generous bosoms?</line>
		<line a="Theres_not">There’s not the meanest spirit on our party</line>
		<line a="Without_a">Without a heart to dare or sword to draw</line>
		<line a="When_Helen">When Helen is defended, nor none so noble</line>
		<line a="Whose_life">Whose life were ill bestow’d or death unfamed</line>
		<line a="Where_Helen">Where Helen is the subject; then, I say,</line>
		<line a="Well_may">Well may we fight for her whom, we know well,</line>
		<line a="The_worlds">The world’s large spaces cannot parallel.</line>
	</speech>
	<speech>
		<speaker role="Hector">Hector</speaker>
		<line a="Paris_and">Paris and Troilus, you have both said well,</line>
		<line a="And_on">And on the cause and question now in hand</line>
		<line a="Have_glozed">Have glozed, but superficially: not much</line>
		<line a="Unlike_young">Unlike young men, whom Aristotle thought</line>
		<line a="Unfit_to">Unfit to hear moral philosophy:</line>
		<line a="The_reasons">The reasons you allege do more conduce</line>
		<line a="To_the_hot">To the hot passion of distemper’d blood</line>
		<line a="Than_to">Than to make up a free determination</line>
		<line a="Twixt_right">‘Twixt right and wrong, for pleasure and revenge</line>
		<line a="Have_ears">Have ears more deaf than adders to the voice</line>
		<line a="Of_any">Of any true decision. Nature craves</line>
		<line a="All_dues">All dues be render’d to their owners: now,</line>
		<line a="What_nearer">What nearer debt in all humanity</line>
		<line a="Than_wife">Than wife is to the husband? If this law</line>
		<line a="Of_nature">Of nature be corrupted through affection,</line>
		<line a="And_that_great">And that great minds, of partial indulgence</line>
		<line a="To_their_benumbed">To their benumbed wills, resist the same,</line>
		<line a="There_is_a">There is a law in each well-order’d nation</line>
		<line a="To_curb">To curb those raging appetites that are</line>
		<line a="Most_disobedient">Most disobedient and refractory.</line>
		<line a="If_Helen">If Helen then be wife to Sparta’s king,</line>
		<line a="As_it">As it is known she is, these moral laws</line>
		<line a="Of_nature_and">Of nature and of nations speak aloud</line>
		<line a="To_have">To have her back return’d: thus to persist</line>
		<line a="In_doing">In doing wrong extenuates not wrong,</line>
		<line a="But_makes">But makes it much more heavy. Hector’s opinion</line>
		<line a="Is_this_in">Is this in way of truth; yet ne’ertheless,</line>
		<line a="My_spritely">My spritely brethren, I propend to you</line>
		<line a="In_resolution">In resolution to keep Helen still,</line>
		<line a="For_tis">For ‘tis a cause that hath no mean dependance</line>
		<line a="Upon_our">Upon our joint and several dignities.</line>
	</speech>
	<speech>
		<speaker role="Troilus">Troilus</speaker>
		<line a="Why_there">Why, there you touch’d the life of our design:</line>
		<line a="Were_it">Were it not glory that we more affected</line>
		<line a="Than_the">Than the performance of our heaving spleens,</line>
		<line a="I_would_not">I would not wish a drop of Trojan blood</line>
		<line a="Spent_more">Spent more in her defence. But, worthy Hector,</line>
		<line a="She_is">She is a theme of honour and renown,</line>
		<line a="A_spur">A spur to valiant and magnanimous deeds,</line>
		<line a="Whose_present">Whose present courage may beat down our foes,</line>
		<line a="And_fame">And fame in time to come canonize us;</line>
		<line a="For_I">For, I presume, brave Hector would not lose</line>
		<line a="So_rich">So rich advantage of a promised glory</line>
		<line a="As_smiles">As smiles upon the forehead of this action</line>
		<line a="For_the_wide">For the wide world’s revenue.</line>
	</speech>
	<speech>
		<speaker role="Hector">Hector</speaker>
		<line a="I_am_yours"><catch />I am yours,</line>
		<line a="You_valiant">You valiant offspring of great Priamus.</line>
		<line a="I_have_a_roisting">I have a roisting challenge sent amongst</line>
		<line a="The_dun">The dun and factious nobles of the Greeks</line>
		<line a="Will_strike">Will strike amazement to their drowsy spirits:</line>
		<line a="I_was_advertised">I was advertised their great general slept,</line>
		<line a="Whilst_emulation">Whilst emulation in the army crept:</line>
		<line a="This_I">This, I presume, will wake him.</line>
	</speech>
	<sd a="Exeunt">
			<exeunt>Exeunt</exeunt>
	</sd>
</section>
