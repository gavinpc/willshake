<section key="1.4">
	<location group="London">London. The Tower.</location>
	<sd a="Enter_Clarence">
			Enter <enter role="George">Clarence</enter>
			and <enter role="Brakenbury">Brakenbury</enter>
		</sd>
	
	<speech>
		<speaker role="Brakenbury">Brakenbury</speaker>
		<line a="Why_looks">Why looks your grace so heavily today?</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="O_I">O, I have pass’d a miserable night,</line>
		<line a="So_full">So full of ugly sights, of ghastly dreams,</line>
		<line a="That_as">That, as I am a Christian faithful man,</line>
		<line a="I_would_not">I would not spend another such a night,</line>
		<line a="Though_twere">Though ‘twere to buy a world of happy days,</line>
		<line a="So_full_of">So full of dismal terror was the time!</line>
	</speech>
	<speech>
		<speaker role="Brakenbury">Brakenbury</speaker>
		<line a="What_was">What was your dream? I long to hear you tell it.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="Methoughts_that">Methoughts that I had broken from the Tower,</line>
		<line a="And_was">And was embark’d to cross to Burgundy;</line>
		<line a="And_in_my">And, in my company, my brother Gloucester;</line>
		<line a="Who_from">Who from my cabin tempted me to walk</line>
		<line a="Upon_the">Upon the hatches: thence we looked toward England,</line>
		<line a="And_cited">And cited up a thousand fearful times,</line>
		<line a="During_the">During the wars of York and Lancaster</line>
		<line a="That_had">That had befall’n us. As we paced along</line>
		<line a="Upon_the_giddy">Upon the giddy footing of the hatches,</line>
		<line a="Methought_that">Methought that Gloucester stumbled; and, in falling,</line>
		<line a="Struck_me">Struck me, that thought to stay him, overboard,</line>
		<line a="Into_the">Into the tumbling billows of the main.</line>
		<line a="Lord_Lord">Lord, Lord! methought, what pain it was to drown!</line>
		<line a="What_dreadful">What dreadful noise of waters in mine ears!</line>
		<line a="What_ugly">What ugly sights of death within mine eyes!</line>
		<line a="Methought_I">Methought I saw a thousand fearful wrecks;</line>
		<line a="Ten_thousand">Ten thousand men that fishes gnaw’d upon;</line>
		<line a="Wedges_of">Wedges of gold, great anchors, heaps of pearl,</line>
		<line a="Inestimable_stones">Inestimable stones, unvalued jewels,</line>
		<line a="All_scatterd">All scatter’d in the bottom of the sea:</line>
		<line a="Some_lay">Some lay in dead men’s skulls; and, in those holes</line>
		<line a="Where_eyes">Where eyes did once inhabit, there were crept,</line>
		<line a="As_twere">As ‘twere in scorn of eyes, reflecting gems,</line>
		<line a="Which_wood">Which woo’d the slimy bottom of the deep,</line>
		<line a="And_mockd">And mock’d the dead bones that lay scatter’d by.</line>
	</speech>
	<speech>
		<speaker role="Brakenbury">Brakenbury</speaker>
		<line a="Had_you">Had you such leisure in the time of death</line>
		<line a="To_gaze">To gaze upon the secrets of the deep?</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="Methought_I_had">Methought I had; and often did I strive</line>
		<line a="To_yield">To yield the ghost: but still the envious flood</line>
		<line a="Kept_in">Kept in my soul, and would not let it forth</line>
		<line a="To_seek">To seek the empty, vast and wandering air;</line>
		<line a="But_smotherd">But smother’d it within my panting bulk,</line>
		<line a="Which_almost">Which almost burst to belch it in the sea.</line>
	</speech>
	<speech>
		<speaker role="Brakenbury">Brakenbury</speaker>
		<line a="Awaked_you">Awaked you not with this sore agony?</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="O_no">O, no, my dream was lengthen’d after life;</line>
		<line a="O_then">O, then began the tempest to my soul,</line>
		<line a="Who_passd">Who pass’d, methought, the melancholy flood,</line>
		<line a="With_that">With that grim ferryman which poets write of,</line>
		<line a="Unto_the">Unto the kingdom of perpetual night.</line>
		<line a="The_first">The first that there did greet my stranger soul,</line>
		<line a="Was_my">Was my great father-in-law, renowned Warwick;</line>
		<line a="Who_cried">Who cried aloud, “What scourge for perjury</line>
		<line a="Can_this">Can this dark monarchy afford false Clarence?”</line>
		<line a="And_so_he">And so he vanish’d: then came wandering by</line>
		<line a="A_shadow">A shadow like an angel, with bright hair</line>
		<line a="Dabbled_in">Dabbled in blood; and he squeak’d out aloud,</line>
		<line a="Clarence_is">“Clarence is come; false, fleeting, perjured Clarence,</line>
		<line a="That_stabbd">That stabb’d me in the field by Tewksbury;</line>
		<line a="Seize_on">Seize on him, Furies, take him to your torments!”</line>
		<line a="With_that_methoughts">With that, methoughts, a legion of foul fiends</line>
		<line a="Environd_me">Environ’d me about, and howled in mine ears</line>
		<line a="Such_hideous">Such hideous cries, that with the very noise</line>
		<line a="I_trembling">I trembling waked, and for a season after</line>
		<line a="Could_not">Could not believe but that I was in hell,</line>
		<line a="Such_terrible">Such terrible impression made the dream.</line>
	</speech>
	<speech>
		<speaker role="Brakenbury">Brakenbury</speaker>
		<line a="No_marvel">No marvel, my lord, though it affrighted you;</line>
		<line a="I_promise">I promise, I am afraid to hear you tell it.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="O_Brakenbury">O Brakenbury, I have done those things,</line>
		<line a="Which_now">Which now bear evidence against my soul,</line>
		<line a="For_Edwards">For Edward’s sake; and see how he requites me!</line>
		<line a="O_God_if">O God! if my deep prayers cannot appease thee,</line>
		<line a="But_thou">But thou wilt be avenged on my misdeeds,</line>
		<line a="Yet_execute">Yet execute thy wrath in me alone,</line>
		<line a="O_spare">O, spare my guiltless wife and my poor children!</line>
		<line a="I_pray">I pray thee, gentle keeper, stay by me;</line>
		<line a="My_soul">My soul is heavy, and I fain would sleep.</line>
	</speech>
	<speech>
		<speaker role="Brakenbury">Brakenbury</speaker>
		<line a="I_will_my">I will, my lord: God give your grace good rest!</line>
		<sd a="Clarence_sleeps">
			Clarence sleeps
		</sd>
		<line a="Sorrow_breaks">Sorrow breaks seasons and reposing hours,</line>
		<line a="Makes_the">Makes the night morning, and the noon-tide night.</line>
		<line a="Princes_have">Princes have but their tides for their glories,</line>
		<line a="An_outward">An outward honour for an inward toil;</line>
		<line a="And_for_unfelt">And, for unfelt imagination,</line>
		<line a="They_often">They often feel a world of restless cares:</line>
		<line a="So_that">So that, betwixt their tides and low names,</line>
		<line a="Theres_nothing">There’s nothing differs but the outward fame.</line>
	</speech>
	<sd a="Enter_the">
		Enter the two Murderers
		<enter role="First_Murderer"/>
		<enter role="Second_Murderer"/>
	</sd>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="Ho_whos">Ho! who’s here?</line>
	</speech>
	<speech>
		<speaker role="Brakenbury">Brakenbury</speaker>
		<line a="In_Gods">In God’s name what are you, and how came you hither?</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="I_would_speak">I would speak with Clarence, and I came hither on my legs.</line>
	</speech>
	<speech>
		<speaker role="Brakenbury">Brakenbury</speaker>
		<line a="Yea_are">Yea, are you so brief?</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<prose/>
		<line a="O_sir">O sir, it is better to be brief than tedious. Show</line>
		<line a="him_our">him our commission; talk no more.</line>
	</speech>
	<sd a="Brakenbury_reads">
		Brakenbury reads it
	</sd>
	<speech>
		<speaker role="Brakenbury">Brakenbury</speaker>
		<line a="I_am_in">I am, in this, commanded to deliver</line>
		<line a="The_noble">The noble Duke of Clarence to your hands:</line>
		<line a="I_will_not_reason">I will not reason what is meant hereby,</line>
		<line a="Because_I_will">Because I will be guiltless of the meaning.</line>
		<line a="Here_are">Here are the keys, there sits the duke asleep:</line>
		<line a="Ill_to">I’ll to the king; and signify to him</line>
		<line a="That_thus">That thus I have resign’d my charge to you.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="Do_so">Do so, it is a point of wisdom: fare you well.</line>
	</speech>
	<sd a="Exit_Brakenbury">
		Exit <exit role="Brakenbury">Brakenbury</exit>
	</sd>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="What_shall">What, shall we stab him as he sleeps?</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="No_then">No; then he will say ‘twas done cowardly, when he wakes.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<prose/>
		<line a="When_he_wakes">When he wakes! why, fool, he shall never wake till</line>
		<line a="the_judgment-day">the judgment-day.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="Why_then_he_will">Why, then he will say we stabbed him sleeping.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<prose/>
		<line a="The_urging">The urging of that word “judgment” hath bred a kind</line>
		<line a="of_remorse">of remorse in me.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="What_art">What, art thou afraid?</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<prose/>
		<line a="Not_to">Not to kill him, having a warrant for it; but to be</line>
		<line a="damned_for">damned for killing him, from which no warrant can defend us.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="I_thought">I thought thou hadst been resolute.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="So_I_am">So I am, to let him live.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="Back_to">Back to the Duke of Gloucester, tell him so.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<prose/>
		<line a="I_pray_thee">I pray thee, stay a while: I hope my holy humour</line>
		<line a="will_change">will change; ‘twas wont to hold me but while one</line>
		<line a="would_tell">would tell twenty.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="How_dost">How dost thou feel thyself now?</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<prose/>
		<line a="Faith_some">‘Faith, some certain dregs of conscience are yet</line>
		<line a="within_me">within me.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="Remember_our">Remember our reward, when the deed is done.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="Zounds_he">‘Zounds, he dies: I had forgot the reward.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="Where_is_thy">Where is thy conscience now?</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="In_the_Duke">In the Duke of Gloucester’s purse.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<prose/>
		<line a="So_when">So when he opens his purse to give us our reward,</line>
		<line a="thy_conscience">thy conscience flies out.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="Let_it">Let it go; there’s few or none will entertain it.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="How_if">How if it come to thee again?</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<prose/>
		<line a="Ill_not_meddle">I’ll not meddle with it: it is a dangerous thing: it</line>
		<line a="makes_a">makes a man a coward: a man cannot steal, but it</line>
		<line a="accuseth_him">accuseth him; he cannot swear, but it cheques him;</line>
		<line a="he_cannot">he cannot lie with his neighbour’s wife, but it</line>
		<line a="detects_him">detects him: ‘tis a blushing shamefast spirit that</line>
		<line a="mutinies_in">mutinies in a man’s bosom; it fills one full of</line>
		<line a="obstacles_it">obstacles: it made me once restore a purse of gold</line>
		<line a="that_I">that I found; it beggars any man that keeps it: it</line>
		<line a="is_turned">is turned out of all towns and cities for a</line>
		<line a="dangerous_thing">dangerous thing; and every man that means to live</line>
		<line a="well_endeavours">well endeavours to trust to himself and to live</line>
		<line a="without_it">without it.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<prose/>
		<line a="Zounds_it">‘Zounds, it is even now at my elbow, persuading me</line>
		<line a="not_to">not to kill the duke.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<prose/>
		<line a="Take_the">Take the devil in thy mind, and relieve him not: he</line>
		<line a="would_insinuate">would insinuate with thee but to make thee sigh.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="Tut_I">Tut, I am strong-framed, he cannot prevail with me,</line>
		<line a="I_warrant">I warrant thee.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<prose/>
		<line a="Spoke_like">Spoke like a tail fellow that respects his</line>
		<line a="reputation_Come">reputation. Come, shall we to this gear?</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<prose/>
		<line a="Take_him">Take him over the costard with the hilts of thy</line>
		<line a="sword_and">sword, and then we will chop him in the malmsey-butt</line>
		<line a="in_the">in the next room.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="O_excellent">O excellent devise! make a sop of him.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="Hark_he">Hark! he stirs: shall I strike?</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="No_first">No, first let’s reason with him.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<wake role="George"/>
		<line a="Where_art">Where art thou, keeper? give me a cup of wine.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="You_shall">You shall have wine enough, my lord, anon.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="In_Gods_name">In God’s name, what art thou?</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="A_man_as">A man, as you are.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="But_not">But not, as I am, royal.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="Nor_you">Nor you, as we are, loyal.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="Thy_voice">Thy voice is thunder, but thy looks are humble.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="My_voice">My voice is now the king’s, my looks mine own.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="How_darkly">How darkly and how deadly dost thou speak!</line>
		<line a="Your_eyes_do">Your eyes do menace me: why look you pale?</line>
		<line a="Who_sent">Who sent you hither? Wherefore do you come?</line>
	</speech>
	<speech>
		<sp>Both</sp>
		<speaker role="First_Murderer"/>
		<speaker role="Second_Murderer"/>
		<line a="To_to">To, to, to—</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="To_murder">To murder me?</line>
	</speech>
	<speech>
		<sp>Both</sp>
		<speaker role="First_Murderer"/>
		<speaker role="Second_Murderer"/>
		<line a="Ay_ay">Ay, ay.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="You_scarcely">You scarcely have the hearts to tell me so,</line>
		<line a="And_therefore_cannot">And therefore cannot have the hearts to do it.</line>
		<line a="Wherein_my">Wherein, my friends, have I offended you?</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="Offended_us">Offended us you have not, but the king.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="I_shall">I shall be reconciled to him again.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="Never_my">Never, my lord; therefore prepare to die.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="Are_you_calld">Are you call’d forth from out a world of men</line>
		<line a="To_slay">To slay the innocent? What is my offence?</line>
		<line a="Where_are">Where are the evidence that do accuse me?</line>
		<line a="What_lawful">What lawful quest have given their verdict up</line>
		<line a="Unto_the_frowning">Unto the frowning judge? or who pronounced</line>
		<line a="The_bitter">The bitter sentence of poor Clarence’ death?</line>
		<line a="Before_I">Before I be convict by course of law,</line>
		<line a="To_threaten">To threaten me with death is most unlawful.</line>
		<line a="I_charge">I charge you, as you hope to have redemption</line>
		<line a="By_Christs">By Christ’s dear blood shed for our grievous sins,</line>
		<line a="That_you_depart">That you depart and lay no hands on me</line>
		<line a="The_deed">The deed you undertake is damnable.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="What_we">What we will do, we do upon command.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="And_he_that">And he that hath commanded is the king.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="Erroneous_vassal">Erroneous vassal! the great King of kings</line>
		<line a="Hath_in_the">Hath in the tables of his law commanded</line>
		<line a="That_thou_shalt">That thou shalt do no murder: and wilt thou, then,</line>
		<line a="Spurn_at">Spurn at his edict and fulfil a man’s?</line>
		<line a="Take_heed">Take heed; for he holds vengeance in his hands,</line>
		<line a="To_hurl">To hurl upon their heads that break his law.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="And_that_same">And that same vengeance doth he hurl on thee,</line>
		<line a="For_false">For false forswearing and for murder too:</line>
		<line a="Thou_didst">Thou didst receive the holy sacrament,</line>
		<line a="To_fight_in">To fight in quarrel of the house of Lancaster.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="And_like">And, like a traitor to the name of God,</line>
		<line a="Didst_break">Didst break that vow; and with thy treacherous blade</line>
		<line a="Unripdst_the">Unrip’dst the bowels of thy sovereign’s son.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="Whom_thou">Whom thou wert sworn to cherish and defend.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="How_canst">How canst thou urge God’s dreadful law to us,</line>
		<line a="When_thou_hast">When thou hast broke it in so dear degree?</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="Alas_for">Alas! for whose sake did I that ill deed?</line>
		<line a="For_Edward_for">For Edward, for my brother, for his sake: Why, sirs,</line>
		<line a="He_sends">He sends ye not to murder me for this</line>
		<line a="For_in">For in this sin he is as deep as I.</line>
		<line a="If_God">If God will be revenged for this deed.</line>
		<line a="O_know">O, know you yet, he doth it publicly,</line>
		<line a="Take_not">Take not the quarrel from his powerful arm;</line>
		<line a="He_needs">He needs no indirect nor lawless course</line>
		<line a="To_cut">To cut off those that have offended him.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="Who_made">Who made thee, then, a bloody minister,</line>
		<line a="When_gallant-springing">When gallant-springing brave Plantagenet,</line>
		<line a="That_princely">That princely novice, was struck dead by thee?</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="My_brothers">My brother’s love, the devil, and my rage.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="Thy_brothers">Thy brother’s love, our duty, and thy fault,</line>
		<line a="Provoke_us">Provoke us hither now to slaughter thee.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="Oh_if">Oh, if you love my brother, hate not me;</line>
		<line a="I_am_his">I am his brother, and I love him well.</line>
		<line a="If_you">If you be hired for meed, go back again,</line>
		<line a="And_I_will">And I will send you to my brother Gloucester,</line>
		<line a="Who_shall">Who shall reward you better for my life</line>
		<line a="Than_Edward">Than Edward will for tidings of my death.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="You_are">You are deceived, your brother Gloucester hates you.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="O_no_he">O, no, he loves me, and he holds me dear:</line>
		<line a="Go_you_to">Go you to him from me.</line>
	</speech>
	<speech>
		<sp>Both</sp>
		<speaker role="First_Murderer"/>
		<speaker role="Second_Murderer"/>
		<line a="Ay_so">Ay, so we will.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="Tell_him_when">Tell him, when that our princely father York</line>
		<line a="Blessd_his">Bless’d his three sons with his victorious arm,</line>
		<line a="And_charged">And charged us from his soul to love each other,</line>
		<line a="He_little">He little thought of this divided friendship:</line>
		<line a="Bid_Gloucester">Bid Gloucester think of this, and he will weep.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="Ay_millstones">Ay, millstones; as be lesson’d us to weep.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="O_do">O, do not slander him, for he is kind.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="Right">Right,</line>
		<line a="As_snow">As snow in harvest. Thou deceivest thyself:</line>
		<line a="Tis_he">‘Tis he that sent us hither now to slaughter thee.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="It_cannot">It cannot be; for when I parted with him,</line>
		<line a="He_huggd">He hugg’d me in his arms, and swore, with sobs,</line>
		<line a="That_he">That he would labour my delivery.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="Why_so_he">Why, so he doth, now he delivers thee</line>
		<line a="From_this">From this world’s thraldom to the joys of heaven.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="Make_peace">Make peace with God, for you must die, my lord.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="Hast_thou">Hast thou that holy feeling in thy soul,</line>
		<line a="To_counsel">To counsel me to make my peace with God,</line>
		<line a="And_art">And art thou yet to thy own soul so blind,</line>
		<line a="That_thou_wilt">That thou wilt war with God by murdering me?</line>
		<line a="Ah_sirs">Ah, sirs, consider, he that set you on</line>
		<line a="To_do">To do this deed will hate you for the deed.</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="What_shall_we">What shall we do?</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="Relent_and">Relent, and save your souls.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="Relent_tis">Relent! ‘tis cowardly and womanish.</line>
	</speech>
	<speech>
		<speaker role="George">Clarence</speaker>
		<line a="Not_to_relent">Not to relent is beastly, savage, devilish.</line>
		<line a="Which_of_you">Which of you, if you were a prince’s son,</line>
		<line a="Being_pent">Being pent from liberty, as I am now,</line>
		<line a="If_two">If two such murderers as yourselves came to you,</line>
		<line a="Would_not">Would not entreat for life?</line>
		<line a="My_friend">My friend, I spy some pity in thy looks:</line>
		<line a="O_if">O, if thine eye be not a flatterer,</line>
		<line a="Come_thou">Come thou on my side, and entreat for me,</line>
		<line a="As_you">As you would beg, were you in my distress</line>
		<line a="A_begging">A begging prince what beggar pities not?</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="Look_behind">Look behind you, my lord.</line>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="Take_that">Take that, and that: if all this will not do,</line>
		<sd a="Stabs_him">
			Stabs him
		</sd>
		<line a="Ill_drown">I’ll drown you in the malmsey-butt within.</line>
	</speech>
	<sd a="Exit_with">
		Exit, with the body
			<exit role="First_Murderer"/>
			<exit role="George"/>
	</sd>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="A_bloody">A bloody deed, and desperately dispatch’d!</line>
		<line a="How_fain">How fain, like Pilate, would I wash my hands</line>
		<line a="Of_this">Of this most grievous guilty murder done!</line>
	</speech>
	<sd a="Re-enter_First">
		Re-enter <enter role="First_Murderer">First Murderer</enter>
	</sd>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="How_now_what">How now! what mean’st thou, that thou help’st me not?</line>
		<line a="By_heavens">By heavens, the duke shall know how slack thou art!</line>
	</speech>
	<speech>
		<speaker role="Second_Murderer">Second Murderer</speaker>
		<line a="I_would_he">I would he knew that I had saved his brother!</line>
		<line a="Take_thou">Take thou the fee, and tell him what I say;</line>
		<line a="For_I_repent">For I repent me that the duke is slain.</line>
		<sd a="Exit">
			Exit
			<exit role="Second_Murderer"/>
		</sd>
	</speech>
	<speech>
		<speaker role="First_Murderer">First Murderer</speaker>
		<line a="So_do_not">So do not I: go, coward as thou art.</line>
		<line a="Now_must">Now must I hide his body in some hole,</line>
		<line a="Until_the">Until the duke take order for his burial:</line>
		<line a="And_when">And when I have my meed, I must away;</line>
		<line a="For_this">For this will out, and here I must not stay.</line>
	</speech>
</section>
