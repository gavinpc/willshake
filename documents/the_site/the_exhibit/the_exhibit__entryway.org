#+PROPERTY: uses_service make_stylesheets

# the exhibit -- the entryway

#+BEGIN_QUOTE
Pray you, come in.
[[https://willshake.net/plays/Oth/3.1#Pray_you_come][/Othello/ 3.1]]
#+END_QUOTE

The entryway is the first thing you see when you arrive at the exhibit.  By this
I mean what you see /immediately/ upon entering, before taking a single step
(i.e. scrolling or following a link).  This coveted place is called "above the
fold," a term taken from the newspaper business.  Since there is so much wonder
and delight in store for those who venture further, it is tempting to crowd the
entryway with all manner of enticements.  And with space being so limited—even
to the size of one's hand—choosing what attractions to promote is a difficult
matter.

This difficulty drives many exhibitors to a paralysis of indecision, which they
then project onto their visitors in the form of a "carousel"—those vertiginous
spotlight rotators that notionally reduce all content, past, present and future,
to a single form (/viz/, the picture and caption).  Meanwhile, one could say that
giving "pride of place" to so many candidates rather robs the place of its
pride.  But one would digress.

A "news" site, or a "feed" would wish to put first those things that change the
most often.  To my mind, this subverts the notion of an hierarchy.  Thus,
willshake always puts foremost whatever will change the least.  And what will
change the least about willshake?  Shakespeare.

Choosing requires emotional energy.  Visitors may feel too indimidated by the
aura and prestige of "Shakespeare" to make choices right away.  Well, the first
one's free.

* space for the entryway

The entryway is part of the exhibit.  Insofar as we expect it to consist of
minimal, navigational content, we put it physically up front (with =prepend=):
#+CAPTION: The entryway is the first thing in the exhibit
#+BEGIN_SRC xml :tangle change_rules/more/home--the-entryway
#exhibit <prepend>
	<div id="entryway" />
</prepend>
#+END_SRC
(As such, this might be considered like a header.)  The 
#+BEGIN_SRC stylus :tangle program/stylus/main-rules-entryway.styl
@import docking
@import order_of_exhibit_layers
#entryway
	hug-parent()                // The entryway fills the viewport.  Always.
	
	//pointer-events none
	z-index $entrywayLayer
	overflow hidden		   // don't reflow in Gecko when children are translated
#+END_SRC
Because of the =z-index= (and other things, I think?) a compositor layer is
created.  The contents of the entryway cannot be gone-between by other layers.

#+BEGIN_NOTE
The point of the =pointer-events none= rule on the entryway (and all of the
"reset" rules on its children) is to allow click-through to the ground.  Without
this rule, the entryway itself would act like an invisible wall blocking pointer
events to anything behind it (even where it is empty).  However, this behavior
is currently unused, as the ground doesn't recognize any user actions.  So if
that continues, I'll get rid of said rules.
#+END_NOTE

* signs in the entryway

The only thing in the entryway are signs.  I'm tempted to say the only thing
/allowed/ in the entryway are signs, but why set myself up to be forsworn?

** acknowledgement of the possibility of other signs in the entryway

There's only one /essential/ sign in the entryway---the one that says
"Shakespeare."  Maybe you've seen it.  (There's also [[file:~/ws/local/documents/the_site/the_beacon.org][the beacon]], but that's part
of the firmament, not the entryway.)
#+CAPTION: There are signs in the entryway
#+BEGIN_SRC xml :noweb tangle :tangle change_rules/more/home--the-entryway-signs
#entryway <prepend>
	<nav id="entryway-signs">
		<<the-Shakespeare-sign>>
	</nav>
</prepend>
#+END_SRC
That =<nav/>= element 
#+BEGIN_SRC stylus :noweb tangle :tangle program/stylus/main-rules-entryway-signs.styl
@import docking
#entryway-signs
	hug-parent()
#+END_SRC

** the Shakespeare sign

This sign is front-and-center in the entryway.  It's almost ridiculous.

In document terms, it's mostly significant as a header.
#+CAPTION: The Shakespeare sign
#+BEGIN_SRC xml :noweb-ref the-Shakespeare-sign
<header id="shakespeare-sign">
  <h1 id="shakespeare-sign-heading">
    <a id="shakespeare-sign-link" href="/#Shakespeare">
      <span class="text">Shakespeare</span>
    </a>
  </h1>
</header>
#+END_SRC

The Shakespeare sign is basically /the/ central affordance on the site.  It sits
just above the center of the entryway.
#+BEGIN_SRC stylus :tangle program/stylus/main-rules-Shakespeare-sign.styl
@import thumb-metrics
@import responsive
#shakespeare-sign
	position absolute
	top 40%
	left 50%
	transform translate(-50%, -50%)
	
	sign_size(thumbs, font = undefined)
		width $thumbRems * thumbs
		height $thumbRems * thumbs
		font-size font if font != undefined
	
	sign_size(thumbs: 1.5, font: 150%)
	scale_sign(min, thumbs, font)
		@media (min-height min) and (min-width min)
			sign_size(thumbs, font)
	
	scale_sign(20rem, 2, 200%)
	scale_sign(25rem, 2.5, 250%)
	scale_sign(30rem, 3.0, 300%)
	scale_sign(35rem, 3.5, 400%)
#+END_SRC

That's just the positioning container; the actual "thing" is a link.
#+BEGIN_SRC stylus :tangle program/stylus/main-rules-Shakespeare-sign.styl
// IT DOTH APPEAR that this h1 is just for semantics.
@import docking
#shakespeare-sign-heading
	hug-parent()
	margin 0					// beat h1 default
	font-size 100%			  // beat h1 default

@import colors
@import arrows
@import pointing
@import centering
@import signs
#shakespeare-sign-link
	// The link fills the sign.
	hug-parent()
	
	//pointer-events auto		 // reset from parent layer
	
	// Doing this here means  it also applies to the down arrow.
	font-weight 200			 // beat h1 default
	line-height 1.2			 // Not really needed, just being explicit
	
	// Round, like a globe
	border-radius 50%
	// The first two box shadows help soften aliasing effects on some parts of
	// the circle.
	//
	// Note that the large shadow does extend the area repainted by hover
	// effects.  You can get around this (at least in Chrome) by putting the
	// shadow on a pseudo-element, but I don't find the extra painting to be
	// detrimental, since the event is infrequent.
	box-shadow \
		0 0 1px $worksColor,
		0 0 3px rgba($worksColor, .6),
		0 .25em 1em -.125em #111
	
	// Sign stuff
	sign-text-color()
	
	// IS THIS kind of a general thing?
	
	// Works sign stuff
	background rgba($worksColor, .8)
	+user_pointing_at()
		background rgba($worksDarkColor, .8)
	
	// Down arrow, below text
	+arrow-after-pointing(down)
		horizontally-center()
		top 70%
	
	> .text
		position absolute
		
		// Center horizontally and (almost) vertically
		top 50%
		left 50%
		transform translate(-50%, -50%)
		margin-top -7%
		
		letter-spacing -.04em
#+END_SRC

* the end of the entryway

What happens if you keep going?  It's worth asking, because some people will.
It's kind of like the "back door"---the thing you reach if you walk past
everything.  Restrooms may be found there, or utility closets that say "staff
only."

In document terms, a "footer" is a section, usually at the end, that implies a
gentle return to the broader context.  Maybe I'll want something like that
later.
#+BEGIN_SRC xml :tangle change_rules/more/home--entryway_footer
#exhibit <append>
	<footer id="entryway-footer"></footer>
</append>
#+END_SRC
In space terms, the footer is just some "breathing room" at the bottom of the
entryway, just as we have at the top.
#+BEGIN_SRC stylus :tangle program/stylus/main-rules--entryway_footer.styl
#entryway-footer
	transform translateY(400vh)
	height 100vh
#+END_SRC
