#+TITLE:finding the web site
#+PROPERTY: subgraph outlets
#+PROPERTY: builds-on URI's

Notes about the indexing of willshake by search engines.  This is something of a
cross-cutting concern, but I use this topic to the extent that the whole affair
can be considered removable.

* robots

Robots are a way of /excluding/ paths from indexing.

As with all things, the simplest approach is not to have one at all.  Just don't
publish things that you don't want indexed.

But while features are under development, it's useful to add locations that
aren't ready yet.

So for that reason, features can just write the necessary files to a specified
location, and they'll get bundled into =robots.txt=.

#+BEGIN_SRC tup
: $(ROOT)/program/robots.txt.list/<all> \
|> ^ bundle robots.txt ^ cat %<all> > %o \
|> $(SITE)/robots.txt
#+END_SRC

The directives will apply to all robots.
#+BEGIN_SRC text :tangle program/robots.txt.list/0_user_agent
User-agent: *
#+END_SRC

Note that =wget= will also respect =robots.txt= when [[file:the_web_platform.org::*pre-rendering%20the%20site][pre-rendering the site]], which
is what you'd want.  /But/, it doesn't recognize wildcards (=*=) the way that Google
does.

* sitemap

Willshake doesn't use a =sitemap.xml= file.  Why not?  In this case, I'd say the
burden of proof is with having one.  It doesn't solve any problem that willshake
has.  From what I've read about them, Sitemap files are just ways to compensate
for deficiencies in the site itself.

* ensure that internal links are valid

I can't stop other people from linking to non-existent places in willshake.net.
But I can at least avoid doing it myself.

To /completely/ eliminate the chance of an internal 404, I'd have to check every
single link in the site, which would mean generating all of the pages.  That's
not worth the effort, considering that Google does it for me.

The real place where bad links have been is an issue is not in generated links,
but in hardcoded links, usually among the documents.  That's because I'm
frequently restructuring and renaming them.  This is done when [[file:reflection/self_documenting/self_documenting.org::*adjust%20link%20addresses][adjusting link
addresses]] in [[file:reflection/self_documenting/self_documenting.org][self documenting]].

At this stage, I'm not doing any redirects when things move.  Outside of the
=/about= documents, the solution is simply to not move things.  I put lots of
thought into the addressing scheme for that reason, and they have been stable
for a long time.  Within the =/about= area, things have not stabilized as much,
but it's also not as much of a concern for indexing (at least not now).

* roadmap

** TODO better not-found handling

This isn't strictly an SEO thing.

There are two main cases of this.  Both are essentially getflow problems.

1. You get a plain text "not found" message from getflow if there is no route.

2. You get a server error (500) if there /is/ a route, but no actual document.
   This happens under =about/something=, for instance, when =something= is not a
   document.  From my perspective, this is still a 404.

* temporary: stub play timeline paths while feature is under development

The play "time regions" have been shelved for a while.  As long as that feature
is shelved, there should be no links to these areas, but they're already in
external indexes.  This should keep them from being revisited.
#+BEGIN_SRC text :tangle program/robots.txt.list/play_time_regions
Disallow: plays/*/upstream
Disallow: plays/*/downstream
#+END_SRC

The same goes for "roles."  There are tons of links to this area, even though
it's been removed for a while.
#+BEGIN_SRC text :tangle program/robots.txt.list/play_roles
Disallow: plays/*/roles
#+END_SRC
