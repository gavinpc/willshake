#+TITLE:using blocks
#+PROPERTY: uses_service make_stylesheets
#+PROPERTY: uses_service make_blocks
#+PROPERTY: uses_service make_script_module

Hey, remember [[file:~/ws/local/documents/blocks.org][blocks]]?

Well, this is where we use them in the site.

* wrapping

Using a transform rule, we can intercept any file listing that's in
=published/blocks=.  Instead of just rendering it as a [[file:program_listings.org][program listing]], it can be
an =<iframe/>=, too.

#+BEGIN_SRC xml :tangle program/publish/org-copy.d/blocks.xsl
<xsl:template
		match="*[@class='org-src-container']
					 [*/*
					 [@data-key='tangle']
					 [starts-with(., 'published/blocks/')]]"
		mode="org-copy">
	
	<xsl:variable name="path"
	              select="*/*[@data-key='tangle']
	                         [starts-with(., 'published/blocks/')]" />
	
	<xsl:variable name="file"
	              select="substring-after($path, 'published/blocks/')" />

	<xsl:variable name="name"
	              select="substring-before($file, '.html')" />

	<iframe src="/static/blocks/{$file}" class="world {$name}" />
	
	<div class="block-source">
		<xsl:copy>
			<xsl:apply-templates mode="org-copy" select="node()|@*" />
		</xsl:copy>
	</div>

</xsl:template>
#+END_SRC


* wiring up

Having a bit of HTML in the document is okay, but by itself, how is it any
better than just embedding the HTML?

Well, in the sandboxed world, you can use scripts and stylesheets that are
isolated from the containing page.[fn:scoped_style]







#+BEGIN_SRC stylus :tangle program/stylus/document-rules--block-sample.styl
$borderWidth = 3px

.accordion
	display flex
	width 100%
	box-sizing border-box
	border $borderWidth solid red

.segment
	box-sizing border-box
	border $borderWidth solid blue
	flex 1 0 auto
#+END_SRC

#+BEGIN_SRC xml :tangle published/blocks/using_sample.html
<head>
	<link rel="stylesheet" href="/static/style/parts/document-rules--block-sample.css" />
</head>
<div class="accordion">
	<div class="segment">a</div>
	<div class="segment">b</div>
	<div class="segment">c</div>
</div>
<script src="/static/script/require-2.2.0.js" data-main="/static/script/block-test.js"></script>
#+END_SRC

#+BEGIN_SRC javascript :tangle scripts/block-test.js
require([], () => {

});
#+END_SRC


* formatting

I don't want there to be borders in the world.
#+BEGIN_SRC stylus :tangle program/stylus/document-rules--blocks.styl
iframe.world
	border 0
#+END_SRC


The code listing is still there.  Of course, it's useful to be able to look at
"the world" next to the code that made it.

But for the moment, let's hide it.

#+BEGIN_SRC stylus :tangle program/stylus/document-rules--blocks.styl
.block-source
	display none
#+END_SRC

* Footnotes

[fn:scoped_style] Blocks would have been a good use case for [[https://developer.mozilla.org/en-US/docs/Web/HTML/Element/style#attr-scoped][scoped style]], but
only Firefox ever implemented it and it was [[https://github.com/whatwg/html/issues/552][removed from the spec]].


