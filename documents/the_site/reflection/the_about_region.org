#+TITLE:the about region
#+PROPERTY: subgraph the_process
#+PROPERTY: uses_service make_change_rules
#+PROPERTY: uses_service make_stylesheets

The /about region/ is a kind of "backstage" area, where you can see willshake's
inner workings.

It stands apart from the rest of the site in that it hosts "meta" content, that
is, content about the site itself.  So in some respect, it belongs to a
different world than the site "proper," though it is part of the domain.  You
might think of it as being on another plane.

* a layer apart

Logically, the about region resides at the =/about= path.
#+BEGIN_SRC xml :tangle program/routes/about
<route path="/about" to="about" />
#+END_SRC

Physically, it lives in a layer that "hovers" beyond the space.  It's not part
of the space proper.
#+BEGIN_SRC xml :tangle change_rules/more/home--exhibit_about
#exhibit <before>

<div id="about-layer" class="scroll-layer">
	<section id="about">
		<h1 id="about-willshake-heading">
			<a href="/about" id="about-willshake-heading-link">about willshake</a>
			<a href="/#entryway" class="exit-link"><span class="exit-link-text" /></a>
		</h1>
		
		<div id="about-main-sheet" class="about-main-sheet">
			<header id="about-willshake-message" class="region-message">
				<p id="mission-statement">Project “willshake” is an ongoing effort to bring
				the beauty and pleasure of Shakespeare to new media.</p>
				<p>Please <a target="_blank"
				href="https://bitbucket.org/gavinpc/willshake/issues?status=new&amp;status=open">report
				problems</a> on the <a target="_blank"
				href="https://bitbucket.org/gavinpc/willshake/issues?status=new&amp;status=open">issue
				tracker</a>.  For anything else, <a
				href="mailto:contact@willshake.net">contact@willshake.net</a></p>
			</header>
			<a href="/#wherever" class="about-hole-link" />
		</div>
	</section>
</div>

<hr class="tty" />

</before>
#+END_SRC

The initial content of the about region is already present when you're at the
entryway.  You just don't see it.  So going there is just a matter of showing
it.
#+BEGIN_SRC xml :tangle change_rules/about.xml
title <html>about willshake.net</html>
#exhibit <class remove="active" />
#about-layer <class add="active" />
#about <class add="open" />
body <class add="about-open" />
#entryway-signs <class add="about-open" />
#+END_SRC


The about region is off screen---backstage, if you will---until it is visited.
It should feel as if you are "backing away" from the rest of the site when going
there.

#+BEGIN_SRC stylus :tangle program/stylus/main-rules-about.styl
@import colors
@import ground_metrics
#the-ground
	transition transform .8s cubic-bezier(.15,.32,.07,.98) !important
	.about-open &
		for x in 10..30
			threshold = x * 100px
			$scale = round(unit($groundImageWidth / threshold, ''), 2)
			@media (max-width threshold)
				transform scale($scale)

#shakespeare-sign
	transition .8s cubic-bezier(.15,.32,.07,.98) !important
	.about-open &
		transform translate(-50%, 0) scale(.4)
#+END_SRC
#+BEGIN_TBD
Except you can't actually see that happen.
#+END_TBD
That's good, but it doesn't quite work by itself.  You also need to /grow/
something, to get a sense of changing depth.
#+BEGIN_SRC stylus :tangle program/stylus/main-rules-about.styl
#willshake-about-sign
	transition .8s cubic-bezier(.15,.32,.07,.98) !important
	.about-open &
		transform scale(1.5)
#+END_SRC
Though, that doesn't quite do it either.  This is still a work in progress.

#+BEGIN_SRC stylus :tangle program/stylus/main-rules-about.styl
@import layers
#about-layer
	scroll-layer()
	z-index 2
	opacity 0
	visibility hidden
	transition visibility 0s .2s, opacity .2s
	.about-open &
		opacity 1
		visibility visible
		transition-delay 0s
		@import layers
		compositing-layer()
#+END_SRC
To the naked eye, the =opacity= change would be enough by itself: the layer fades
in and out as needed.  The =visibility= change is added to ensure that the layer
doesn't use a compositing layer when it's not visible, which it would do at
least in Safari iOS. [fn:Huisman_CSS_visibility_transition]

* within the region

#+BEGIN_SRC stylus :tangle program/stylus/main-rules-about.styl
@import exit-links
@import colors
@import docking
@import region_headings
@import transition-timings
@import typesetting

#about
	// Use most of the vertical space.  The gap is just a reminder that the
	// title box belongs to what's above it; otherwise it would look like the
	// heading of something below (and offscreen).
	//min-height 90%					// fallback for no calc
	min-height calc(100vh - 4rem)
	color white

.about-main-sheet	
	display flex
	flex-direction column
	height 100vh
	align-items center
	justify-content center
	.region-message
		max-width $readingRems
#+END_SRC

** the header

#+BEGIN_TBD
I'm not sure where to put this.  Right now it's still rendered, but hidden.
#+END_TBD
#+BEGIN_SRC stylus :tangle program/stylus/main-rules-about.styl
#about-willshake-heading
	display none  // TEMP
	region_heading()
	has-exit-link()
	dock-bottom()
	
	background $aboutColor

#about-willshake-heading-link
	region_heading_link()
	background inherit
#+END_SRC

* sheets

The about region is set apart from the site.  This is indicated by a [[file:~/ws/local/documents/the_site/layers.org][layer]]
between you and the rest of the space.  It's a transluscent layer, letting you
see that the site is still there.
#+BEGIN_SRC stylus :tangle program/stylus/main-rules-about.styl
@import colors
.about-sheet
	margin 0
	background rgba($aboutColor, .8)
#+END_SRC

** a hole in the sheet

To make it even more apparent that this is a layer in front of the site, why not
punch a hole in it?
#+BEGIN_SRC stylus :tangle program/stylus/main-rules-about.styl
$holeX = 20%
$holeY = 80%

.about-hole-link
	position absolute
	top $holeY
	left $holeX
	box-shadow inset 0 0 1em #222, inset 2px 1px 3px #111
	transition background .2s
	
	@import pointing
	+user_pointing_at()
		background rgba(white, .2)

punch-hole($holeRadius)
	
	.about-main-sheet
		background radial-gradient(circle at $holeX $holeY,
		    transparent 0,
		    transparent ($holeRadius * .95),
		    #222 $holeRadius,
		    rgba($aboutColor, .8) $holeRadius)
	
	.about-hole-link
		@import signs
		circle($holeRadius * 2)
		margin-top (0 - $holeRadius)
		margin-left (0 - $holeRadius)

punch-hole(15vmin)
@media (min-width 35rem) and (min-height 30rem)
	punch-hole(20vmin)
#+END_SRC

* the about sign

The "about" sign is simply a link to the about region.
#+BEGIN_SRC xml :noweb tangle :tangle change_rules/more/home--the-entryway-about_sign
#entryway <prepend>
	<a id="willshake-about-sign" href="/about">
		<span class="text">about</span>
	</a>
</prepend>
#+END_SRC
What does it look like?
#+BEGIN_SRC stylus :tangle program/stylus/main-rules-about-sign.styl
@import arrows
@import colors
@import thumb-metrics
@import signs
@import centering
@import pointing

#willshake-about-sign
	pointer-events auto		 // reset from parent layer
	
	position absolute
	top 0
	right 0
	
	// The about sign is a circle, a sphere
	circle(width: $thumbRems)
	box-shadow 0 0 .5em #111
	opacity .7
	
	background $aboutColor
	sign-text-color()
	
	// Apply font-size to this (instead of .text) so that it also applies to the
	// arrow.
	font-size 150%
	.text
		full-center()
	
	+user_pointing_at()
		background #666
	
	+arrow-before-pointing(up, size: .75em)
		horizontally-center()
		top -.5em
#+END_SRC

** TODO where should this thing go?

I feel like I've tried everything.  I don't like where it is now.  It's too hard
to hit, it's too easy to miss, it's in the way when you don't expect it to be
there at all.

* Footnotes

[fn:Huisman_CSS_visibility_transition] Brian Huisman has a decent writeup about
this technique (particularly regarding the need for the transition delay) at
"[[http://www.greywyvern.com/?post%3D337][CSS3 transitions using visibility and delay]]" (greywyvern.com, 2011).
