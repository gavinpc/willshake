#+TITLE:program listings
# THIS has to be shelved when self-documenting is shelved
#+PROPERTY: subgraph the_process
#+PROPERTY: builds-on self_documenting

Look around: this place is filled with code blocks.

* block navigation

While it's very nice to have the code integrated with the documents, it's also
important to see how the block fits into the larger system.

One consequence of splitting up the program into pieces that can go anywhere and
everywhere, is that it's not necessarily obvious how the pieces fit together.

#+BEGIN_SRC xml :tangle program/publish/org-copy.d/program-listings.xsl
<<index_listings>>
<<program_listings>>
#+END_SRC

This means that the "structural" headers (=tangle= and =noweb-ref=) will need unique
HTML =id='s, so that they can be link targets.
#+CAPTION: Determine header ID's
#+BEGIN_SRC xsl :noweb-ref index_listings
<xsl:key name="tangle" match="li[@data-key='tangle']" use="." />
<xsl:key name="noweb-ref" match="li[@data-key='noweb-ref']" use="." />

<xsl:template mode="id" match="li">
	<xsl:variable name="indexed" select="count(key(@data-key, .))" />
	
	<!-- Only indexed blocks have ID's at all -->
	<xsl:if test="$indexed > 0">
		<xsl:value-of select="translate(., './', '__')" />
		
		<!-- Number blocks if there are several. -->
		<xsl:if test="$indexed > 1">
			<xsl:value-of
					select="concat('_',
									1 + count(preceding::li[@data-key = current()/@data-key]))" />
		</xsl:if>
	</xsl:if>
</xsl:template>
#+END_SRC

Now, the ID has to be put on the header, of course.  And we also want to put
links to the next and previous blocks, if there are any, so that you can follow
that particular thread.
#+BEGIN_SRC xsl :noweb-ref program_listings
<xsl:template mode="org-copy" match="li[@data-key]">
	<xsl:variable name="key" select="@data-key" />
	
	<li class="src-header">
		<xsl:apply-templates mode="org-copy" select="@*" />
		
		<xsl:apply-templates
				mode="link"
				select="(preceding::li[@data-key = $key][.=current()])[last()]">
			<xsl:with-param name="type" select="'previous'" />
		</xsl:apply-templates>
		
		<!-- You can almost use the link mode template for this -->
		<xsl:variable name="id">
			<xsl:apply-templates select="." mode="id" />
		</xsl:variable>
		<xsl:variable name="text" select="translate(., '_', ' ')" />
		<xsl:if test="$id != ''">
			<a href="#{$id}" id="{$id}" class="src-meta-link self">
				<xsl:value-of select="$text" />
			</a>
		</xsl:if>
		<xsl:if test="$id = ''">
			<xsl:value-of select="$text" />
		</xsl:if>
		
		<xsl:apply-templates
				mode="link"
				select="(following::li[@data-key = $key][.=current()])[1]">
			<xsl:with-param name="type" select="'next'" />
		</xsl:apply-templates>
	</li>
</xsl:template>
#+END_SRC

This is a relatively general way to make a link to a source block.
#+BEGIN_SRC xsl :noweb-ref program_listings
<xsl:template mode="link" match="li[@data-key]">
	<xsl:param name="type" select="." />
	<xsl:variable name="id">
		<xsl:apply-templates mode="id" select="." />
	</xsl:variable>
	<xsl:text> </xsl:text>
	<xsl:if test="$id != ''">
		<a href="#{$id}" class="src-meta-link {$type}">
			<span class="text"><xsl:value-of select="$type" /></span>
		</a>
	</xsl:if>
	<xsl:text> </xsl:text>
</xsl:template>
#+END_SRC

** export source code block headers

#+BEGIN_TBD
This is an "incidental complexity" section that deals with an external system
that doesn't work the way it should.  At some point, I'll formally tag such
sections.
#+END_TBD

The metadata about each code block comes from its "header line".  In the Org
source, it looks like this
#+BEGIN_SRC org
,#+BEGIN_SRC stylus :tangle program/stylus/main-rules-document.styl
#+END_SRC
or this
#+BEGIN_SRC org
,#+BEGIN_SRC stylus :noweb-ref program_listing_styles
#+END_SRC
This tells what file is written, or what placeholder is filled, by the code
block that follows.  It's essential for mapping the documents to the "actual"
program.

Unfortunately, Org throws that information away during export.  This problem was
reported by John Kitchin on the Org-mode mailing list, where it was agreed that
this destructive behavior indeed "plays merry hell" with parts of the system,
and could well be avoided.  But as of this writing it "remains an
imperfection."[fn:orgmode_export_src_headers]

Professor Kitchin came up with a solution: simply collect the headers when they
are available, and re-insert them during export.[fn:Kitchin_org_export] I
adapted that gentleman's solution for this system.
#+CAPTION: Hack to export source block headers
#+BEGIN_SRC emacs-lisp :tangle program/publish/setup/program_listings.el :group emacs_setup
(require 'cl)
(require 'org-element)
(require 'ox)

(defun ws/src-block-key (src-block)
  (org-element-property :value element))

(defun ws/init-src-params (backend)
  (org-export-expand-include-keyword)
  (setq ws/src-params
		(org-element-map (org-element-parse-buffer) 'src-block
		  (lambda (element)
			`(,(ws/src-block-key element) .
			  ,(org-element-property :parameters element))))))

(defun ws/add-src-block-metadata (orig-fn &rest args)
  (let* ((html (apply orig-fn args))
		 (src-block (first args))
		 (code (ws/src-block-key src-block))
		 (record (assoc code ws/src-params))
		 (params-line (cdr record))
		 (most-params (read (concat "(" params-line ")")))
		 ;; For some reason language isn't included in the parameters list
		 (language (plist-get (cadr src-block) :language))
		 (params (plist-put most-params :language language))
		 (metadata
		  (concat "<ul class='src-metadata'>"
				  (loop for (key value) on params if (keywordp key)
						concat
						(format
						 "<li data-key='%s'>%s</li>"
						 (substring (symbol-name key) 1) value))
				  "</ul>")))
	(unless record (message "no match for %s" code))
	(replace-regexp-in-string
	 "\\`\\(<[^>]*>\\)?"
	 (concat "\\1" metadata) html)))

(add-hook 'org-export-before-processing-hook 'ws/init-src-params)
(advice-add 'org-html-src-block :around #'ws/add-src-block-metadata)
#+END_SRC
The spirit of the original remains, but not much of the code.  In particular,
this can run /before/ the Org file is open, which is when our other setup is done.
To its discredit, this version abandons the lexical scoping of Mr. Kitchin's
version, which it can get away with since every export is done in a new Emacs
instance.

The metadata is listed at the beginning of the source block container.  It's
rendered as valid HTML, mainly because =xsltproc= is so fussy.

*** support selective export

The original version also used a simple list to store the headers, which works
if every block is being exported.  If not, you need a way to /look up/ the header
for the block you're processing.  I ended up using an associative array with the
block's /content/ as the key.  This seems a bit heavy-handed, but it works (99% of
the time), and the =:begin= property ,which would have been a lighter-weight
choice, changes during the export.  Even the content (i.e. the code itself) is
not 100% reliable, since it needs to remain exactly as-written, and Org
sometimes takes a free hand with formatting.
#+BEGIN_SRC emacs-lisp :tangle program/publish/setup/program_listings.el
(setq org-src-preserve-indentation t)
#+END_SRC
Without this setting, some whitespace would be stripped between the time the map
is collected and the time it is used.

**** BUG tangle header still not set sometimes

Even =org-src-preserve-indentation= doesn't cut it if you have a line that is only
whitespace.  On such blocks you may not get a tangle header, because the source
as exported won't match the source as written (since Org will strip out the
leading whitespace).

*** support includes

Babel strips out the parameters at some point during the export, so we have to
collect them pretty early.  But if we do it /too/ early, then =INCLUDE='s won't have
been expanded yet, so blocks from included documents won't be found.  It turns
out that =org-export-before-parsing-hook= is too late, and
=org-export-before-processing-hook= is too early.  In fact, the latter hook is
called /right before/ =org-export-expand-include-keyword= in Org's source.  So we
just call that ourselves.  It'll get called again and not do anything (except
waste time).  I mean, this whole thing is a hack, anyway.  Hopefully it'll get
fixed upstream at some point.

* placeholders

/Placeholders/ are special pieces of code that are looked up elsewhere.  Usually
this is done (in literate programming) because the details of that part are
better discussed elsewhere.  Most languages provide ways to create "chunks"
(such as functions) that can be lexically removed from their usage.
Placeholders are useful for dislocating code when those constructs can't be used
for some reason (which I admit is not the case in the following example).
#+BEGIN_EXAMPLE python
def Success():
	print("Have an idea")
	<<magic>>
	print("Profit!");
#+END_EXAMPLE
Org has extensive support for "noweb" placeholders.  They can be parameterized
and invoked like functions.  We throw all that out the window by using our own
tangle.  In our system, they're nothing but placeholders.  And Org exports these
placeholders as dead text, which drives me nuts.  Clearly, they should be links.

The problem with placeholders is that they are not part of any language, yet
they can be mixed into any language.  Fortunately, Pygments makes this pretty
straightforward to deal with.  You can mix the existing language lexers with a
custom one that only recognizes the noweb constructs and delegates everything
else (by marking it as =Other=).  =NowebLexer= is just such a lexer for noweb-style
placeholders
#+BEGIN_SRC python :noweb-ref noweb_lexer
from pygments.lexer import RegexLexer
from pygments.token import *

class NowebLexer(RegexLexer):
    tokens = {
        'root' : [
            (r'[^<]+', Other),
            (r'<\<.*?>>', Name.Label),
            (r'.+', Other) ] }
#+END_SRC
#+BEGIN_NOTE
Don't unescape that =<= in the regex above---unless you're ready to kill the build
process.  The processing of /that line/ by the modified lexer will hang without
the escape (which causes it not to match itself).  I'm not sure why.
#+END_NOTE
In conjunction with the =DelegatingLexer= used above, that causes =<<placeholders>>=
(including the brackets) to be treated as =Name.Label='s, which appear in the
formatted output with class =nl=.  So it's straightforward to find them during the
XML transform phase.
#+BEGIN_SRC xsl :noweb-ref program_listings
<xsl:template mode="org-copy"
							match="span[@class='nl'][starts-with(., '&lt;&lt;')]">
	<xsl:call-template name="placeholder-link">
		<xsl:with-param name="ref" select="substring(., 3, string-length(.) - 4)" />
	</xsl:call-template>
</xsl:template>
#+END_SRC
(Yeah, it doesn't check the end of the string, so it could conceivably get some
false positives.)  The =noweb= blocks are already indexed, so it's simple to make
them into links.
#+BEGIN_SRC xsl :noweb-ref program_listings
<xsl:template name="placeholder-link">
	<xsl:param name="ref" select="." />
	
	<xsl:variable name="id">
		<xsl:apply-templates select="key('noweb-ref', $ref)[1]" mode="id" />
	</xsl:variable>
	
	<a href="#{$id}" class="src-placeholder">
		<span class="open">&lt;&lt;</span>
		<xsl:value-of select="translate($ref, '_', ' ')" />
		<span class="close">>></span>
	</a>

</xsl:template>
#+END_SRC

Now, let's make the things presentable.
#+BEGIN_SRC stylus :noweb-ref program_listing_styles
@import fonts
@import arrows
@import centering
@import document-map-colors

.src-placeholder
	font-size 200%
	
	display inline-block
	margin-top .2em
	padding .5em
	
	background $documentMapColor
	border-radius .25em
	
	body-font()
	color #FFE
	
	position relative
	padding-right 1.5em
	+arrow-after-pointing(right, size: .75em)
		vertically-center()
		right -.4em
	
	> .open
	> .close
		display none
#+END_SRC

* overview (list of listings)

S'posing as we wanted an overview of the programs created in a particular
document.

#+BEGIN_SRC xml :tangle program/routes/document_listings
<route path="/about/{document}/listings" to="document_listings" />
#+END_SRC

At that address, we'll have a different navigation of the program available.

#+BEGIN_SRC xml :tangle change_rules/document_listings.xml
.about.document <after>
<div class="document-listings-layer">
	<nav class="document-listings-nav">
		<h2 class="document-listings-heading">
			<span>list of listings</span>
			<a href="/about/{document}" class="exit-link">
				<span class="exit-link-text">close</span>
			</a>
		</h2>
		<xslt input="/static/doc/about/{document}.xml"
					name="document_listings" />
	</nav>
</div>
</after>
#+END_SRC

The transform will basically just pull the document listing from the main
document (except that hang on, it wasn't written, right?).

#+BEGIN_SRC xsl :tangle program/transforms/document_listings.xsl
<<index_listings>>

<xsl:template match="/">
	<xsl:call-template name="list-of-listings" />
</xsl:template>
#+END_SRC

The index of course will link to each of the items of a given type.  This
template creates such an index for any given header.
#+BEGIN_SRC xsl :tangle program/transforms/document_listings.xsl
<xsl:key name="src-headers"
				 match="*[@class='src-metadata']/li" use="@data-key" />

<xsl:template name="list-of-listings">
	<xsl:param name="key" select="'tangle'" />
	
	<xsl:variable
			name="listings"
			select="key('src-headers', $key)
							[generate-id() = generate-id(key($key, .)[1])]" />
	
	<p>
		This document creates
		<xsl:value-of select="count($listings)" />
		<xsl:value-of select="$key" />
		<xsl:if test="count($listings) != 1">s</xsl:if>
	</p>
	
	<ul class="src-listings">
		<xsl:for-each select="$listings">
			<li class="src-listings-item">
				<xsl:variable name="id">
					<xsl:apply-templates mode="id" select="." />
				</xsl:variable>
				<a href="#{$id}" class="src-listing-link">
					<xsl:value-of select="." />
				</a>
			</li>
		</xsl:for-each>
	</ul>
</xsl:template>
#+END_SRC

#+BEGIN_TBD
Disabling this for now.  See [[*BUG%20issues%20with%20the%20navigation][issues with the navigation]].
#+END_TBD
#+BEGIN_SRC xml :xtangle ../change_rules/more/about_document--listings
.document <prepend>
<a href="/about/{document}/listings">
	Here is a link to my wonderful listings.
</a>
</prepend>
#+END_SRC

#+BEGIN_SRC stylus :noweb-ref program_listing_styles
@import docking
@import signs
@import exit-links
@import pointing
@import document-map-colors
@import plain_list

.document-listings-layer
	dock-right(position: fixed)
	min-width 20%
	max-width 45%
	overflow-y scroll
	// Good question http://stackoverflow.com/q/17547063
	display flex

.document-listings-nav
	margin auto
	background rgba($documentMapColor, .9)
	color $signTextColor
	box-shadow 0 0 1em #111

.document-listings-heading
	font-size 200%
	font-weight 200
	margin-top 0
	has-exit-link()

.src-listings
	plain-list()

.src-listing-link
	display block
	padding .5em 1em
	font-size 150%
	+user_pointing_at()
		background $documentMapDarkColor

#+END_SRC

* presentation

Several computer languages are used in these documents.  To make this code
easier for human eyes to scan, it will be "dyed" using a program called
Pygments.

Org will attempt to do syntax highlighting using the =htmlize= package.  That
needs to be turned off in order to use Pygments.  I do that by replacing
=org-html-fontify-code= with a simple XML-escaping function.  For some reason
using =CDATA= makes the content disappear.
#+BEGIN_SRC emacs-lisp :tangle program/publish/setup/program_listings.el
(defvar xml-entity-alist '((?< . "&lt;") (?& . "&amp;")))

(defun xml-escape (text)
  (replace-regexp-in-string
   "[<&]"
   '(lambda (s) (cdr (assoc (string-to-char s) xml-entity-alist)))
   text))

(advice-add 'org-html-fontify-code :override
			'(lambda (code lang) (xml-escape code)))
#+END_SRC

#+BEGIN_NOTE
You might ask, why not just use Pygments right there?  Well, that would be a
very good question.  There is some difficulty in having Emacs call subprocesses
from Tup, although I'm not sure why it's an issue here since the subprocess
doesn't need to read anything through the FUSE file system.  But even when I
work around that (by hacking Tup), the process just takes far longer than the
present method, which does everything in one pass while Pygments is loaded.
#+END_NOTE

#+BEGIN_SRC python :tangle program/publish/format_code_blocks :group post_processors
import sys, io
from lxml import html
from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter

# For placeholders
from pygments.lexer import DelegatingLexer
<<noweb_lexer>>
<<language_fallbacks>>

input_stream = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')
doc = html.parse(input_stream)
root = doc.getroot()
formatter = HtmlFormatter()

if root is None: exit(0)

for listing in root.xpath('.//div[@class="org-src-container"]'):
    language = listing.xpath('string(.//*[@data-key="language"])')
    known_language = fallbacks.get(language, language)
    language_lexer = get_lexer_by_name(known_language)
    lexer = DelegatingLexer(type(language_lexer), NowebLexer)
    block = listing.find('pre')
    code = block.text
    if code is None: continue
    new_block = html.fromstring(highlight(code, lexer, formatter))
    listing.replace(block, new_block)
    new_block.set('class',
                  new_block.get('class')
                  + ' ' + block.get('class')
                  + ' ' + known_language)
    
sys.stdout.write(html.tostring(doc).decode('utf-8'))
#+END_SRC

Pygments doesn't (yet) know about all of the languages that we use.  For those,
we have to pick a "closest match."
#+BEGIN_SRC python :noweb-ref language_fallbacks
fallbacks = {
    "stylus": "sass",
    "tup": "make",
    "xsl": "xslt",
    "org": "text",
    "dot": "text",
    "conf": "apacheconf"
}
#+END_SRC

Now, those code blocks will still not look like anything special until some
style rules are applied.  Pygments ships with a number of predefined styles,
which can be generated as CSS.

#+BEGIN_SRC tup
: $(ROOT)/Tupfile.ini |> pygmentize -f html -S "lovelace" > %o \
|> $(CSS)/main-rules--program-listings.css
#+END_SRC

#+BEGIN_SRC stylus :tangle program/stylus/document-rules--program-listings.styl
<<program_listing_styles>>
#+END_SRC

This source block controls the formatting of source blocks, so if you want to
know what it does, start by looking at how it's formatted.
#+BEGIN_SRC stylus :noweb-ref program_listing_styles
.org-src-container
	pointer-events none
	> label
		display block
		font-style italic

// Still won't get caption, but I guess it's not needed.
@import document_colors

.org-src-container
	> .src-metadata
	> .src
		pointer-events auto
.src
	display inline-block
	background tint($documentColor, 50%)
	box-sizing border-box
	padding 1.5em
	margin-left -.25em // line up a little better with box shadow
	max-width calc(100% - .25em)
	border-radius 1em
	// Using an aggressively warm color, otherwise it looks really too bluish on
	// some screens.  You only see the fringe, so it's very subtle.
	box-shadow 0 0 1.5em -.7em rgba(#840, .8)
	
	// CSS3.  Seems the default is 8, which is way too big.  Better solution may
	// be to replace tabs with spaces on the way in, this is more flexible (for
	// the browsers that support it).  As of Firefox 49, the prefix is needed.
	-o-tab-size 2
	-moz-tab-size 2
	tab-size 2

// I don't know what to do with these
li[data-key="language"]
	display none
#+END_SRC
Source blocks are presented as a material that floats on top of the document.
Happily, Org renders a wrapper around the =pre= that contains the code.  We make
that =pre= an inline block, so that its box is only as wide as its content.  Given
that the document color is off-white, the =tint= gives the source block a sort of
enamel color.  Doing this, in fact, makes the off-whiteness of the document much
more noticeable, for better or worse.

#+BEGIN_TBD
This will apply to other wide blocks, but at the moment there's no general way
to address their content (other than a wildcard selector, which, no).
#+END_TBD
The =pointer-events= are turned off on =.org-src-container= because it usually
contains an invisible area to the right of the actual source block.  It's
possible that there can be affordances on the right side of the document (such
as the title on the map).

** traversal

We added a number of links so that you can follow around the various source
blocks.  But they look very plain right now.

#+BEGIN_SRC stylus :noweb-ref program_listing_styles
@import plain_list
@import fonts
@import arrows
@import pointing
@import signs
@import centering

.src-metadata
	body-font()
	plain-list()

.src-meta-link
	display inline-block
	vertical-align middle
	padding .25em
	border-radius .25em
	font-size 150%
	text-decoration none !important // sue me
	color $signTextColor
	
	background $documentMapColor
	+user_pointing_at()
		background $documentMapDarkColor
	
	&.previous, &.next
		position relative
		circle(2em)
		padding 0
		.text
			display none
	
	&.previous:before, &.next:after
		position absolute
		horizontally-center()
	
	&.previous
		+arrow-before-pointing(up, size: .75em)
			bottom 40%
	
	&.next
		+arrow-after-pointing(down, size: .75em)
			top 40%

#+END_SRC


** comment stuff

Here is some further formatting of source blocks... a somewhat ill-conceived
attempt to make program comments (that is, comments that are still part of the
actual source code) look like regular document text.  This may be a way to
salvage some touch of humanity in the tangled documents themselves, which are
rapidly being bereft of what comments were left.

#+BEGIN_SRC stylus :noweb-ref program_listing_styles
@import fonts

.org-comment-delimiter
.org-comment
	line-height 1.1

.org-comment
	book-font()
	font-size 1rem

.org-comment-delimiter
	display inline-block
	width 0
	overflow hidden
	
	margin-top 1em
	margin-bottom 1em
	.org-comment ~ &
		margin-top 0
		margin-bottom 0
	
	&:before
		content ''
		border-left .2em solid #333
#+END_SRC

* roadmap

** TODO link to where placeholder is filled

It's essential that placeholders in the code link to where they are defined.
But this should go both ways: the placeholder definitions should also link (one
way or another) to where they are /used/.

** PROPOSAL show jagged top and/or bottom for listings with other segments

Perforated, torn, whatever would indicate that it's not standalone.

* BUG issues with the navigation

It barely works at all right now, but in any case:
- [ ] I don't think it really needs a separate page
- [ ] would like if you could swipe it back and forth
  - [ ] and preferably "naturally"
  - [ ] but can still force hide
  - [ ] and still work naturally with non-touch screen
- [X] it should be centered vertically when shorter than the screen
- [ ] oh and the links don't work in a lot of cases
- [ ] and they say the wrong thing (include "next" and "previous")
- [ ] and they aren't really grouped by file
  - [ ] but maybe could show subgroups, or at least a count of parts
- [X] and the shadow is on the wrong thing

(Actually, removing the listings is working on Firefox and Chrome
Desktop... maybe it's a mobile bug in getflow.)

* Footnotes

[fn:orgmode_export_src_headers] [[https://lists.gnu.org/archive/html/emacs-orgmode/2014-09/msg00646.html]["update on missing :parameters in code blocks"]],
emacs-orgmode discussion group, September 2014

[fn:Kitchin_org_export] It was either [[http://kitchingroup.cheme.cmu.edu/blog/2014/09/22/Showing-what-data-went-into-a-code-block-on-export/]["Showing what data went into a code block
on export"]] or [[http://kitchingroup.cheme.cmu.edu/blog/category/orgmode/][this post]].
