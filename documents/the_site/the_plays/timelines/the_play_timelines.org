#+TITLE:the play timelines
#+PROPERTY: subgraph Shakespeare
#+PROPERTY: builds-on the_scenes
#+PROPERTY: uses_service make_stylesheets
#+PROPERTY: uses_service make_change_rules
#+PROPERTY: uses_service make_script_module

This document creates a foundation for timelines of the plays.

* motivation

People want to see timelines of Shakespeare's plays.  I know that for sure.  For
many years, the main way that people have ended up on [[https://willshake.net][willshake.net]] has been by
searching for the timeline of a particular play.  (I know this because of Google
Webmaster Tools.)

But that's not the motivation for this /per se/.

When we get to know a play, we create a "mental map" of it.  Our thoughts that
we have about the play often "take place" in this space, where we're more or
less sensitive to the main events in the story, and their proximity at any given
point.

The purpose of these timelines is not to fill out a picture of the play's
action, but to create a minimal canvas on which such a picture could be drawn.
It should also serve as a navigational tool.

** synoptic views

#+BEGIN_TBD
This pertains to maps somewhat as well.
#+END_TBD

How long is a Shakespeare play?  I don't mean how long does a show last, I mean,
how /long/ is a play if you roll out the the text?

Most of the plays are about twenty-five pages in the Folio.  But that's two
columns of text on each page, so fifty columns.  Let's generously assume a
foot-high page, since willshake's layout is much more spacious than the Folio's.
That's fifty feet high.  So if you didn't have to "scroll," you'd be looking at
a "five-story" column of text.  Is it a coincidence that the plays are divided
into five acts?  Yes.  Get that idea out of your head.

The /text/ is the main view of the play.  Everything else is optional.  So as a
starting point, it makes sense to ask, why shouldn't the text be the basis for a
"bird's eye" view of the play?

In conclusion, a synoptic view based on the "typeset" text itself is not deemed
useful (or practical) enough that any attempt is made here.  There is only one
"scale" at which you can view the text, namely, full size, and you can only be
"at some point" in the text at any given time.  Hence the timelines.

* prior art

Sometimes, people get excited about computers.  The thing that's exciting about
computers is that they're /general-purpose/.  Once you "grok" that, it's easy to
get excited because this /thing can do literally anything you tell it/ (yes,
literally).

Some people in this situation will think of ideas, things that they want to try.
Let's /visualize/ something!  But what?  What do we have for input?

And many of these people turn to Shakespeare.  Maybe thanks to the fact that the
MOBY has been available for so long, and it seems so temptingly structural.

Consider, for example, "[[http://web.mit.edu/6.mitx/www/#Shakespeare][Shakespeare Visualizations]]," an ostensibly
non-functional demo project at =mit.edu= which describes itself this way:
#+BEGIN_QUOTE
This tool is designed to offer a data visualization tool for the works of
William Shakespeare. The goal is to allow students, readers, and actors to
understand aspects of Shakespeare's work (such as character line distribution,
word distribution, etc.) through intuitive, customizable, easy to use graphs and
charts. This project focuses primarily on being able to search through specified
texts for words that correlate to a certain image. For example, a student would
be able to search for all the occurences of the words
'moon','lunar','cycles','wane','wax','crescent' and see the distribution of
words by charachter [sic] and play.
#+END_QUOTE
It's hard not to think of that scene in /Dead Poet's Society/ when Robin Williams
glibly draws a coordinate plane on the board while one of the students reads the
bloodless introduction to their poetry text, in which the greatness of a sonnet
is described as the product of its formal technique and the weightiness of its
subject.

I'm not here to criticize other people's work.  But if that "demo" demonstrates
anything, it is only how utterly misguided are such efforts.

The idea that any "tool" could "allow students, readers, and actors to
understand aspects of Shakespeare's work" is a perfect example of the old
saying, "When all you have is a hammer, everything starts to look like a nail."

* address

Each play has its own timeline, which has its own location (URL), so you can
visit it specifically.
#+BEGIN_SRC xml :tangle program/routes/play_a_timeline
<route path="/plays/{play}/timeline" to="play_timeline" />
#+END_SRC
That said, the timeline will also integrate with the scenes.
#+BEGIN_NOTE
The route file is called =play_a_timeline= because it has to sort before
=play_section=, which create the wildcard route =/play/{play}/{section}=.  If that
were first, then =timeline= would be interpreted as a scene number.
#+END_NOTE

* discoverability

But how do you get to it?  Indeed, how do you know that it exists?  For the
moment, let's just stick a link on the main sheet.
#+BEGIN_SRC xml :tangle change_rules/more/play--timeline.xml
.site-play-description <append>
	<a href="/plays/{play}/timeline" class="play-main-timeline-link">
		timeline of <play-title />
	</a>
</append>
#+END_SRC
I'll probably want to put an anchor on there to make it go to the top.  At any
rate, it should be a big obvious thing to touch.
#+BEGIN_SRC stylus :tangle program/stylus/play-rules--timeline-link.styl
@require colors
@require signs
@require pointing
.play-main-timeline-link
	display block
	pointer-events auto			// override ancestor
	sign-text-color()
	padding .5em 1em
	border-radius 2px
	background $worksColor
	+user_pointing_at()
		background $contextColor
#+END_SRC

* structure

The timeline is a synoptic view.  It can't be as big as the play itself, of
course, since that would defeat its purpose.  But it also can't be a single
page, because then it couldn't tell a story.

In terms of space, the timeline should be in the neighborhood of, say, five or
ten paces long.  If you were standing back, you could see it all at once, but
not in all of its detail.  In order to read the details, you have be close
enough to the timeline that only part of it is in focus.  So it's a synoptic
view, but no so synoptic that you can hold it in your hand (like a [[file:~/ws/local/documents/the_site/the_plays/maps/maps_of_the_plays.org][map]]).  The
timeline is something that you can "walk around."

Touching the timeline of course "takes you" to the part of the play being
represented.  In that case, the play itself becomes your focus.  But you don't
completely lose sight of the timeline.

As such, the timeline is "present" whether you are visiting it for its own sake,
or whether you are visiting a scene.  So the timeline must be part of the basic
structure of each play.

But where?  The short answer, is that the timeline represents a /new layer/ in the
play area.  Why?  Because it must be scrollable, and the other scrollable layers
are already devoted to something else (namely, the entryway and the scene text).
#+BEGIN_TBD
At the moment, =scroll-layer= is not necessary for functioning (that I know of).
It's just needed for the restoration of scroll position during development.
#+END_TBD
#+BEGIN_SRC xml :tangle change_rules/more/play--timeline.xml
#play-{play}-entryway-layer <after>
<div id="play-{play}-timeline-layer" class="play-timeline-layer scroll-layer">
	<section id="play-{play}-timeline" class="play-timeline">
		<header class="play-timeline-header">
			<h1 class="play-timeline-heading">
				<a href="/plays/{play}#back-to-{play}-choices"
					 class="play-timeline-back-link" />
				<a href="/plays/{play}/timeline">
					Timeline <span class="tty">of <i><play-title /></i></span>
				</a>
			</h1>
		</header>
		<div id="play-{play}-timeline-body" class="play-timeline-body" />
	</section>
</div>
</after>
#+END_SRC
Apart from the title, the timeline is empty, and it will stay that way in this
document.  Remember, other features can add things to it, and it's assumed that
at least one of them will---or else the title would be lying!

#+BEGIN_TBD
But this is going to have to be coordinated with the generated height/position
right?
#+END_TBD
The problem is that when you enter a scene, the timeline is offset by a
transform that doesn't know about the extra heading.  The per-scene transform
simply offsets the entire content of the /layer/ enough to get the 
#+BEGIN_SRC stylus :tangle program/stylus/play-rules--timeline_layer.styl
@import paper
//.play-timeline-header
	// just shouldn't naturally exceed play-timeline-section's top margin
	//height $headerHeight
.play-timeline-heading
	position relative
	z-index 1 // get above body
	margin-top 3rem // beat h1 default
	font-size 150% // beat h1 default
	padding-left .25em
	font-weight normal // beat h1 default
@import docking
.play-timeline-body
	hug-parent()

// mostly copied from play dismiss link
@import signs
@import arrows
@import centering
.play-timeline-back-link
	text-align left // reset from parent
	display inline-block
	position relative
	float left
	circle(1.2em)
	margin-right .125em
	box-shadow 0 0 4px -2px // #222
	sign-text-color()
	
	position relative
	+arrow-after-pointing(left, size: .4em)
		vertically-center()
	
	background rgba(black, .3)
	@import pointing
	@import colors
	+user_pointing_at()
		background $contextColor


#+END_SRC
Features that add content to the play timeline should add it to the "body"
element.



The timeline layer is initially invisible.  That is, it's present in the play
entryway, but it's "offstage."  The timeline has its own "home page," and of
course visiting that will reveal it.
#+BEGIN_SRC xml :tangle change_rules/play_timeline.xml
<title>Timeline of <play-title /> @ willshake.net</title>
#play-{play}-timeline-layer <class add="in-view" />
#play-{play}-timeline-layer <class add="open" />
#play-{play}-timeline <class add="open" />
#play-{play}-entryway-layer <class remove="active" />
#+END_SRC
#+BEGIN_TBD
Rename =active=.  To what?
#+END_TBD
When you visit the timeline, the entryway changes to its "fixed" state, where
instead of being scrollable, it always shows the main sheet (just as when you
visit a scene).

When you visit a scene, the timeline is also called into action, though not as
the dominant layer.
#+BEGIN_SRC xml :tangle change_rules/more/play_section--z-play_timeline.xml
#play-{play}-timeline-layer <class add="in-view" />
#play-{play}-timeline <class add="in-scene-{play}-{translate(section, '.', '-')}" />
#play-{play}-timeline <class add="open" />
#play-{play}-timeline <class add="in-scene" />
.timeline-{play}-{translate(section, '.', '-')} <class add="open" />
#+END_SRC
#+BEGIN_NOTE
Those =class= rules targeting the timeline are split as an optimization.  Because
the second rule doesn't change in different contexts, it can be skipped entirely
for many transitions.  See [[file:~/ws/local/documents/getflow/getflow.org::*shortcuts][shortcuts]] in [[file:~/ws/local/documents/getflow/getflow.org][getflow]].
#+END_NOTE
Also, note that when you're in a scene, you tell it what scene you're in so that
it can adjust itself accordingly.

** appearance / layers

The play's timeline resides in the foremost layer.  Most of the time, it is not
scrollable, and its contents are positioned by rules.

#+BEGIN_SRC stylus :tangle program/stylus/play-rules--timeline_layer.styl
@import layers
.play-timeline-layer
	// See "smooth transitions" below.  This would interfere where
	// `scroll-behavior` is supported.
	scroll-layer(smooth: false, left: 14vw)
	pointer-events none
	-webkit-overflow-scrolling touch
	
	// The whole layer is shaded.  This means it covers most of the screen, but
	// it might as well, because there's no way to click through a scrolling
	// layer to one below.  (At least not across all browsers and devices.)
	// Text will sit directly on this area, so it can't be too transparent.
	//
	// Reverting to narrow timeline again.
	//background rgba(tint($paperColor, 35%), .85)
	//box-shadow 0 0 .5em -.25em #111
	
	// TESTING: Beat default from `scroll-layer` --- but that itself was just for hacking iOS
	&:not(.active)
		pointer-events auto
	
	z-index 1
	
	&:not(.open)
		// The play timeline is only scrollable when it's "open".  This also
		// supports optimized transform transitions of its content (in Gecko).
		overflow hidden
		// The layer itself only needs pointer input when it's scrollable.  For
		// purposes other than scrolling, child elements can override this.
		pointer-events none
	
	// For lack of a better idea, the play timeline (like the text of the play),
	// hangs out "stage right" when it's not in view.
	transition transform .25s
	&:not(.in-view)
		// perspective is needed to force compositing on some browsers; see
		// "layers".
		transform perspective(1px) translateX(100%)
#+END_SRC
#+BEGIN_TBD
That area won't be usable for "wheel" or (probably) touch scrolling of the
timeline.  I have a plan for all that.  When you touch the map, the timeline can
be scrolled to the corresponding area.  And the mouse blind spot can be overcome
with scroll events.
#+END_TBD
Note that the play timeline layer does *not* extend all the way to the left edge
of the screen.  Instead, it leaves a gap for anything that might be there, such
as, say, a map of the play.

#+BEGIN_TBD
This is an old note and needs to be re-verified.  At any rate, I think this is
covered under [[file:~/ws/local/documents/the_site/layers.org][layers]].
#+END_TBD
#+BEGIN_NOTE
Note that we do /not/ use =-webkit-overflow-scrolling touch= here.  This would
enable "momentum" scroll on Chrome and Safari mobile, but it also causes the
play timeline to stutter during scroll.
#+END_NOTE

#+BEGIN_SRC stylus :tangle program/stylus/play-rules--timeline.styl
@import docking
.play-timeline
	dock-top()
	<<play_timeline_rules>>
#+END_SRC

#+BEGIN_TBD
This is only needed if the layer really need to turn off pointer events.  Does
it?
#+END_TBD
The play timeline can be touched, even when the /layer/ is ignoring touches.
#+BEGIN_SRC stylus :noweb-ref play_timeline_rules
pointer-events auto
#+END_SRC

#+BEGIN_TBD
I haven't verified that this is still true (about the reflows), but the
=padding-bottom= is necessary for basic correctness (to prevent the last scene
from clipping prematurely), as long as the timeline has an explicit height.

If this is true, then it belongs with [[*optimizations%20for%20fast%20transitions][optimizations for fast transitions]].
#+END_TBD	
This technique eliminates the reflows (layouts) in Gecko that would be due to
the translation positioning of children.  The padding is necessary to ensure
that the open scene is visible, since the timeline, which has an explicit
height, is actually larger than that when a scene is open.
#+BEGIN_SRC stylus :noweb-ref play_timeline_rules
overflow hidden
padding-bottom 6rem // without this, it seems to be offset by 3rem
&.in-scene
	padding-bottom 100vh
#+END_SRC

The play timeline is an "atomic" layer; nothing can interpose its contents.
This creates a stacking context so that we can isolate the layering order within
this area.  (Yes, the =transform= also does this.)
#+BEGIN_SRC stylus :noweb-ref play_timeline_rules
z-index 0
#+END_SRC

The play timeline is moved so that the current scene is in view.  That is done
by scene-specific rules in a generated stylesheet.  This prevents us from
transitioning between =transform: none= and another value.
#+BEGIN_SRC stylus :noweb-ref play_timeline_rules
transform translateY(0)
#+END_SRC

The actual transforms are defined on a per-section basis in a generated
stylesheet.
#+BEGIN_SRC stylus :noweb-ref play_timeline_rules
@import transition-timings
default-transition('transform')
transition-duration .5s
#+END_SRC

* the flow

This section deals with the time-oriented dimension of the timeline.  In
practice, that is the vertical axis, but it doesn't need to be.  The same
questions (and answers, I imagine) would apply to horizontal timelines.

The timeline is initially empty.  Other features will dream up things to put
into the timeline.  In principle, they can arrange those things however they
want inside of the timeline, which is just a big box.  But the assumption is
that content will be arranged /by scene/.  So it should be easy to add
scene-oriented material to the timeline, simply by associating it with a scene
number---i.e., without having to worry about its coordinates.

The styles generated here will do that job.
#+BEGIN_NOTE
See also [[file:~/ws/local/documents/the_site/the_plays/maps/maps_of_the_plays.org][maps of the plays]], which does a simliar thing.
#+END_NOTE

** the height of the timeline

How tall is the timeline?

I've already said that it will be taller than the screen, but by how much?  It
can be useful to size things relative to the screen: I do something similar in
the entryways, where I want to guarantee that each "sheet" being presented can
be viewed in its entirety.  Here, for example, I could say that each play has
five acts, so it should be five screens high.  That's just an example; of
course, the act divisions are mostly arbitrary.  But so is screen size---and at
any rate, it's completely unknown in advance.  Meanwhile, the purpose of the
timeline is to help create a /mental image/.  Making all of the timelines the same
size---which the plays are not---would reduce their distinctiveness, making them
individually less memorable.

So the timeline should be defined in /absolute/ units that are influenced by the
/length of the play/.

Excactly how that is done must, for the moment remain somewhat arbitrary.  Right
now, the timeline is defined as roughly one twentieth of the length of the play
in =rem='s.  More exactly, this is the "padded" length, where each scene gets a
fixed amount of "padding" lines so that very short scenes still have a usable
amount of space.

Conceptually, the timeline sections represent a continuous flow, and if you had
one box for each scene, they should look and act as if they were in fact
flow-positioned.

But they're not.  Again, the timeline is a canvas, and you can put anything
anywhere.  If you want to have overlapping sections, or sections spanning
multiple scenes, or skip some scenes entirely, you can do that.  So the layout
rules that apply to timeline sections always assume absolute positioning.
#+BEGIN_NOTE
Besides all that, flow positioning of large elements can be slow during
transitions.  This method also supports efficient painting and compositing.
#+END_NOTE
#+BEGIN_SRC stylus :tangle program/stylus/play-rules--timeline_sections.styl
.play-timeline-section
	position absolute
	left 0
	right 0
	<<timeline_section_rules>>
#+END_SRC

#+BEGIN_SRC python :tangle program/timelines/make_styles
import sys
from lxml import etree

# Index the scenes
play = etree.parse(sys.argv[1]).getroot()
play_key = play.get('key')
padding = 100
magic_rems = 20
sections = [{
    "key": section.get('key'),
    "lines": section.xpath('count(.//line|.//sd)')}
            for section in play.findall('.//section')]
preceding = 0
for section in sections:
    section['preceding'] = preceding
    padded = section['lines'] + padding
    section['height'] = padded / magic_rems
    preceding += padded

for section in sections:
    offset = '{:.2f}rem'.format(section['preceding'] / magic_rems)
    dom_key = play_key + '-' + section['key'].replace('.', '-')
    
    # Timeline offset by scene
    print(('.play-timeline.in-scene-{0}{{'
           + '-webkit-transform:translateY(-{1});'
           + 'transform:translateY(-{1});'
           + '}}').format(dom_key, offset))
    
    # Scene height (in timeline view)
    print(('.timeline-{0}{{'
           + 'height:{1:.2f}rem;'
           + '}}').format(dom_key, section['height']))
    
    # Placement and height of scenes.
    print(('.timeline-{0},.open.timeline-{0}+.play-timeline-section{{'
           + 'top:{1};'
           + '}}').format(dom_key, offset))
#+END_SRC
The sharp-eyed reader might note that the selector
=.open.timeline-{0}+.play-timeline-section= appears to incorrectly put two
successive scenes in the same position.  It does, but that's intentional.  The
reason will be clear in a moment.

One scene can be "open" at a time.  When you're visiting a scene, its timeline
section becomes more or less "full screen."  I say "more or less" because some
margin is left on each end.  This can help serve as a reminder that the section
is, in fact, part of a longer flow.
#+BEGIN_SRC stylus :noweb-ref timeline_section_rules
$margin = 5rem
margin-top $margin
&.open
	height "calc(100vh - %s)" % ($margin * 2)
	//z-index 1
	margin-top $margin
#+END_SRC
But what about the sections that come /after/ the open one?  Because the sections
are all absolutely positioned, changing the height of the open scene will not
push following scenes down.  They still have a =top= setting that assumes the
previous section had its proportional height.  In order to maintain the
perception of a continuous set of scenes, the scenes following the open one need
to be adjusted downward.  Pushing them away by the size of the screen is
sufficient to ensure that they're out of view.
#+BEGIN_SRC stylus :noweb-ref timeline_section_rules
transform translateY(0)
&.open ~ &
	transform translateY(100vh)
#+END_SRC
Because of that =.open+= rule that was generated, the scene /immediately/ following
the open scene would thus begin exactly at the bottom of the screen, just out of
view.  To bring it back flush with the open scene, it has to be unshifted by the
size of the margin.
#+BEGIN_SRC stylus :noweb-ref timeline_section_rules
&.open + &
	margin-top (0 - $margin)
#+END_SRC

The style generator uses the full text of the plays as input (only because it
needs the line counts).  Since those "collated" versions are generated by
another subsystem, we can't iterate over them directly.  However, since we /can/
iterate over the plays' "regular" files, there's an easy workaround.
#+BEGIN_SRC tup
: foreach $(PLAYS)/* \
  | $(PROGRAM)/timelines/make_styles \
    $(ROOT)/collated/plays/<all> \
|> ^o play timeline styles for %B^ \
   $(PROGRAM)/timelines/make_styles $(ROOT)/collated/plays/%b > %o \
|> $(CSS)/plays/%B--timeline.css
#+END_SRC

** optimizations for fast transitions

#+BEGIN_TBD
NOW THAT sections have a complete non-performance-related reason for being
absolutely-positioned, I don't know how much of the "speed strategy" is that.
The one point I'm not settled on is the height of the timeline.
#+END_TBD

The open scene also sits in front of others as necessary.  This is part of the
speed strategy described above.  Because the sections are all explicitly
positioned and their movements not strictly coordinated, they may overlap during
transitions.  They will never overlap in their end states.

Since the children are explicitly positioned (see the "speed strategy" mentioned
elsewhere), this box won't have a natural height.  But it is still expected to
add up to the height of its children.
#+BEGIN_TBD
Why is that?  What would happen if the timeline didn't have an explicit height?
Wouldn't the layer still make them scrollable?  Or does this have to do with the
"speed strategy"---wanting to support =overflow hidden= on the container?
#+END_TBD

#+BEGIN_SRC python :tangle program/timelines/make_styles
# Play timeline height
print('#play-%s-timeline {height:%drem;}'
      % (play_key, sum([_['height'] for _ in sections])))
#+END_SRC

** another optimization?


#+BEGIN_TBD
This smooths over an abrupt jump when transitioning scenes, but it requires many
repaints for its duration.

It also causes lagginess (in this area) when the viewport is being vertically
resized.  That's not terribly common, though, and it's not too bad.

I'm not sure now what the above note means.  This is evidently necessary for
smooth transitions, and sure, it requires more computation.
#+END_TBD
#+BEGIN_SRC stylus :noweb-ref timeline_section_rules
@import transition-timings
transition-property height transform
transition-duration $defaultTransitionDuration
transition-timing-function $defaultTransitionTiming
#+END_SRC

* a note on content

As mentioned earlier, other features may add content to the timeline.  Just as
the timeline can be viewed in different ways, content can be added to it in
different ways.  These choices will impact the resources needed to move around
in the play, so care should be taken to use the most efficient option.

Generally, content should be added to the timeline only when visiting the
timeline specifically.  This way, the timeline "page" is dedicated to its
purpose, and adding material to it will only impact the page where it is seen.

However, part of the timeline is visible even when visiting a scene,
specifically so that it can be used.  

Finally, it is possible to add content for all scenes.  This should be done
sparingly, as it impacts all pages in the play area.

* smooth transitions

Smooth transitions between scenes and the play timeline require some special
attention.  There's no "natural" way to do this, since the timeline must switch
from absolute to fixed positioning while it may be partially scrolled.

#+BEGIN_NOTE
To elaborate: currently, getflow doesn't support any way to specify an element's
scrolled position, other than by scrolling to an anchor.  Any /other/ scrollable
layer will start out scrolled to the top, just as it would be on a new page
view.  (And parity between "GET" and "flow" is getflow's "job.")  So if you have
a transition where a layer changes from scrollable to not-scrollable (such as
the play timeline's layer), getflow doesn't help achieve smooth transitions.
You need to specially script it.
#+END_NOTE
#+BEGIN_SRC xml :tangle change_rules/more/home--play_timelines.xml
<require module="play_timelines" />
#+END_SRC
This module is currently only used for one thing: smooth transitions between the
timeline and scenes.
#+BEGIN_SRC javascript :tangle scripts/play_timelines.js
define(['getflow'], getflow => {
	<<smooth_scene_transitions>>
})
#+END_SRC

The basic idea is that a scene timeline should /remain in view/ when you're going
to and from the timeline.  It should be easy to follow it with your eyes.
Otherwise, it wouldn't be clear what was happening.

In other words, there shouldn't be much movement.  In effect, all of the
following work is really about /preventing work/.  Without these measures, the
browser will move the timeline far more than necessary, then run it back when it
realizes where it's supposed to end up.  This is called "thrashing" and I hate
it.

Going to the play timeline from the scene, we want the scene timeline to stay in
view.  Since it will shrink from full height to the (presumably) smaller height
used in the play timeline, later scenes may come into view.  And maybe we'd want
the current scene somewhat offset (rather than flush to top), so that prior
scenes also come into view.  Regardless of what happens, it must be fluid enough
so that it's clear that the current scene remains continuously in view.

Going to a scene from the play timeline (where it will occupy the full viewport
height), we want the target scene to move no more than necessary.  In the play
timeline view, all of the sections are in the flow and scrollable in a dedicated
layer; whereas when a scene is open, that layer is not scrollable and the scene
is brought into view by translating the timeline contents.  So in the scene
view, the timeline layer should not be scrolled at all, as it would be on a new
request.

In either case, the strategy here is to switch between scrolling of the layer
and translation of the content.  The switch should be instant and produce no
visible change.  The rest should be handled "naturally."

Okay, let's get started.  First, a few preliminaries.

Not naming any names, but there are still browsers that require a prefix on the
=transform= property.
#+BEGIN_SRC javascript :noweb-ref smooth_scene_transitions
const TRANSFORM = 'transform' in document.body.style
	  ? 'transform'
	  : '-webkit-transform';
#+END_SRC
This trick is also going to require knowing how much an element is vertically
translated.  Webkit models the matrix so you can get its properties without
parsing[fn:SO_15423338], but the method used here is more cross-browser.  This
is a "pure" function, except that calling =getComputedStyle= can trigger layout.
#+BEGIN_SRC javascript :noweb-ref smooth_scene_transitions
function getComputedTranslateY(ele) {
	
	const style = window.getComputedStyle(ele),
		  
		  transform = style.transform
		  || style.webkitTransform
		  || style.mozTransform,
		  
		  [_, _3d, matrix] = transform.match(/matrix(3d)?\((.+)\)/);

	return matrix?
		parseFloat(matrix.split(', ')[_3d? 13 : 5]) : 0;
}
#+END_SRC
This is a little "cross-browser" method of batching updates---although if you
don't have =requestAnimationFrame=, you're probably screwed anyway.
#+BEGIN_SRC javascript :noweb-ref smooth_scene_transitions
function run_dom_update(fn) {
	if (window.requestAnimationFrame)
		window.requestAnimationFrame(fn);
	else
		window.setTimeout(fn, 1);
}
#+END_SRC

The trick is all about a little sleight of hand involving a container and some
content.  You want to switch between two states so that the change is not
noticeable.  But the difference is in how the content is positioned, and by how
far the layer is scrolled.
#+BEGIN_SRC javascript :noweb-ref smooth_scene_transitions
function set_scroll_and_translate(layer, content, scroll, y, also) {
	run_dom_update(() => {
		const style = content.style;
		
		// Don't animate this change.  The whole point is to set these
		// offsetting properties instantly.
		style.transition = 'none';
		style[TRANSFORM] = 'translateY(' + y + 'px)';
		layer.scrollTop = scroll;
		
		if ('function' == typeof also) also();
		
		style[TRANSFORM] = null;
		style.transition = null;
	});
}
#+END_SRC

#+BEGIN_TBD
The actual router would be helpful to have here.  Also, see "stage
manager"---the whole point of which is to model cases like this.
#+END_TBD
This trick has to be done at the /beginning/ of the transition.  Waiting until the
end won't work because the browser might already be working on applying the
changes.
#+BEGIN_SRC javascript :noweb-ref smooth_scene_transitions
const PLAY_ROUTE = /^\/plays\/(...?)\/([\w\d\.]+?)([\/\?#]|$)/i;
getflow.on_going((from_path, to_path) => {
	<<maybe_assist_timeline_transition>>
});
#+END_SRC
Each time the user navigates, we have to check whether this special case
applies.  It applies, of course, when moving between a play's timeline and one
of its scenes.
#+BEGIN_SRC javascript :noweb-ref maybe_assist_timeline_transition
if (!from_path || !to_path)
	return;

const [_, from_play, from_section] = PLAY_ROUTE.exec(from_path) || [],
	  [__, to_play, to_section] = PLAY_ROUTE.exec(to_path) || [],
	  from_timeline = from_section == 'timeline',
	  to_timeline = to_section == 'timeline';

if (!from_play || !to_play || from_play != to_play)
	return;

const timeline = document.getElementById('play-' + to_play + '-timeline'),
	  layer = document.getElementById('play-' + to_play + '-timeline-layer');

if (!timeline || !layer) return;  // this "shouldn't happen"
#+END_SRC
Now, for the magic.
#+BEGIN_SRC javascript :noweb-ref maybe_assist_timeline_transition
function scene_class(play, scene) {
	return 'in-scene-' + play + '-' + scene.replace('.', '-');
}

if (to_timeline && !from_timeline)
	
	// Scene to timeline: switch offset from content's translation to parent's
	// scroll
	set_scroll_and_translate(layer, timeline, -getComputedTranslateY(timeline), 0, () => {
		timeline.classList.remove(scene_class(from_play, from_section));
	});

else if (from_timeline && !to_timeline)
	
	// Timeline to scene: Switch offset from parent's scroll to content's
	// translation
	set_scroll_and_translate(layer, timeline, 0, -layer.scrollTop, () => {
		timeline.classList.add(scene_class(to_play, to_section));
	});
#+END_SRC
Besides swapping the offets, it's also necessary to set the classes /in advance/
that will be set later by the change rules.  Those same class changes will be
made by getflow almost immediately after this, but if anything causes the script
to yield before that happens, the browser will start doing its dirty work.  This
whole trick only works if the changes are all done synchronously.


* parts in scene

The purpose of this transform is to emit an element tree containing the unique
parts in a scene (which it takes as input).  It's the caller's job to convert
this into a node-set.

This transform is also referenced by [[file:maps_of_the_plays.org][maps of the plays]] and [[file:play_storyboards.org][play storyboards]], but
both of those are shelved, and to wit, it's only really needed here.

#+BEGIN_SRC xsl :tangle program/transforms/parts-in-scene.xsl
<xsl:template match="section" mode="parts-in-scene">
	<xsl:variable name="role-refs">
		<xsl:copy-of select=".//*[@role]" />
	</xsl:variable>
	
	<xsl:for-each select="ex:node-set($role-refs)
												/*[not(@role = preceding::*/@role)]">
		<part role="{@role}" />
	</xsl:for-each>
</xsl:template>
#+END_SRC

* agenda
** BUG sometimes opens to wrong offset in scene view

Seems to be more often in the last scene.

** TODO make special index for overall timeline

The overall timeline needs essentially the entries and exits from /all/ sections.

The thing to do here would be to include the relevant =sd='s with their line
numbers.  But the thing that produces it will have to change pretty
dramatically.  At some point I had represented these as "presences," and it may
be best to use this concept in the intermediate format.

But then the question is, do you include that in the main play index (that will
get downloaded whenever the play is visited), or do you do a separate index for
this, since it's not used on the play page itself?
* Footnotes

[fn:SO_15423338] "[[http://stackoverflow.com/a/15423338/4525%20][getting current CSS3 Translate in animation]]", StackOverflow
