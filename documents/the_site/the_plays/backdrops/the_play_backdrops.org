#+TITLE:the plays backdrops
#+PROPERTY: subgraph Shakespeare
#+PROPERTY: builds-on the_plays
#+PROPERTY: uses_resources
#+PROPERTY: uses_service make_stylesheets
#+PROPERTY: uses_service make_diagrams

Each play is unique.

Each play is like its own world.  Going to one of the plays should feel like
entering another world.

/Backdrops/ are sometimes used in theaters to set a scene.  We use carefully
selected artwork to help create that same effect.

Here we define specially selected and positioned background images for plays.

#+BEGIN_SRC xml :tangle change_rules/more/home--space_curtain
#xdocument-background-layer
<after>
	<div class="space-curtain" />
</after>
#+END_SRC

#+BEGIN_SRC xml :tangle change_rules/more/play--backdrop
#document-background-layer <class add="has-image" />
#document-background <class add="play-{play}" />
#+END_SRC


* what do they look like?

#+BEGIN_SRC stylus :noweb-ref support
@import colors
@import docking
@import transition-timings

$nightFallTime = .5s
$nightFallDelay = .25s
$nightFallTotalTime = $nightFallTime + $nightFallDelay
#+END_SRC

When entering a document (currently, a play), it is as if night falls.

#+BEGIN_NOTE
It's definitely expensive to make two more compositor layers, one for each of
these pseudo-elements.  But it's the difference between their timing that
creates the "nightfall" effect, in which the top fades in faster than the
bottom.

Also, at least in Safari, removing that =transform= layers doesn't really save any
memory: either you get 2 layers (for the pseudo-elements) with an N-megabyte
buffers, or you get one layer for the main element with a 2N-megabyte buffer.
Meanwhile, I don't see that the opacity transition is hardware accelerated on
/any/ browser.  I may need to try dedicated elements for this.
#+END_NOTE

#+BEGIN_SRC stylus :noweb-ref support
#xdocument-background-layer
	// Gecko transition optimization hack.
	overflow hidden
	
	&:before
	&:after
		content ''
		hug-parent()
		opacity 0
		// This keeps the pseudo-elements below the actual backdrop.
		z-index -1
		transform translateZ(0)
	
	&:before
		background $nightSkyColor
		transition opacity $nightFallTime $nightFallDelay $easeInOutQuart
	
	// Emulate “falling” effect with opacity by fading in a transparency
	// gradient faster than the solid color.
	&:after
		background-image linear-gradient($nightSkyColor, $nightSkyColor 4rem, transparent)
		transition opacity ($nightFallTotalTime / 2) $easeInOutQuart
		
	&.active:before
	&.active:after
		opacity 1

#document-background-layer
	&:before
		content ''
		@import docking
		hug-parent()
		background-image linear-gradient($nightSkyColor, $nightSkyColor 4rem, transparent)
		transition opacity .5s
		opacity 0
	
	&.active:before
		opacity 1

.space-curtain
	@import order-of-main-layers
	overflow hidden
	$fadeHeight = 33vh
	position fixed
	top 0
	left 0
	right 0
	height s('calc(100vh + %s)' % $fadeHeight)
	transition transform .25s
	transform-origin top center
	transform translate(0, -100%)
	background-image linear-gradient(to top, transparent, $nightSkyColor $fadeHeight)
	z-index $documentBackgroundLayer - 1
	
	
	&.active
		transform translate(0, 0)



#+END_SRC

#+BEGIN_SRC stylus :noweb-ref support
.background
	hug-parent()
	<<background_rules>>
#+END_SRC

The backdrops will be individual images, not patterns.
#+BEGIN_SRC stylus :noweb-ref background_rules
background-repeat no-repeat
#+END_SRC

Most backdrops are single, full-width images.
#+BEGIN_SRC stylus :noweb-ref background_rules
background-size 100%
#+END_SRC

#+BEGIN_TBD
As things stand the diptychs end up with ugly gaps at some sizes.  You'd need to
know the height of the shorter image---or rather, the image that would be
shorter when it is scaled to 50% of the viewport---and use media rules
("breakpoints") accordingly.
#+END_TBD
Some backdrops are diptychs.  This rule has no impact on single-image
backgrounds, other than to state the default.

#+BEGIN_SRC stylus :noweb-ref background_rules
background-position top left, top right
#+END_SRC

#+BEGIN_TBD
THIS STILL doesn't fade out, though.  When leaving the play, the class defining
the background is removed and thus it disappears abruptly.
#+END_TBD
The backdrop fades in.
#+BEGIN_SRC stylus :noweb-ref background_rules
transition opacity 1s ($nightFallTotalTime + .2s),
    background-size $defaultTransitionDuration,
    transform $defaultTransitionDuration
opacity 0
.has-image > &
	opacity 1
#+END_SRC

Parallax effect when going to time regions (upstream and downstream), as in the
main entryway.
#+BEGIN_SRC stylus :noweb-ref background_rules
transform translateX(0)
&.downstream
	transform translateX(-20%)
&.upstream
	transform translateX(20%)
#+END_SRC

* types of backdrops

Here is a case where a homoiconic language might come in handy.  As it is, we're
going to do this in Stylus.

#+BEGIN_SRC stylus :tangle program/stylus/play-rules--backdrops.styl
<<support>>
<<full_screens>>
<<portraits>>
<<banners>>
<<diptychs>>
<<missing>>
#+END_SRC

Each play may have one or more backdrop images.  This "mixin" lets us associate
an [[file:images.org][image]] (by its [[file:images.org::*naming%20images][key]]) with a play (by its key).
#+BEGIN_SRC stylus :noweb-ref support
play-backdrop(play, images...)
	
	.background.play-{play}
		$images = ''
		for image, number in images
			$images += ',' if number > 0
			$images += "url('/static/images/%s@fullscreen.jpg')" % s(image)
		background-image s($images)
		{block}
#+END_SRC
By including ={block}=, we can also add more specific rules.

This lets us assign rules to a set of play backdrops as a group simply by
listing their keys together.  We'll do this once for each type of backdrop, in
the sections that follow.
#+BEGIN_SRC stylus :noweb-ref support
backdrops(plays...)
	$selector = ''
	for play, number in plays
		$selector += ',' if number > 0
		$selector += ".background.play-%s" % s(play)
	{$selector}
		{block}
#+END_SRC

** full-screens

Maybe it would be best if we had full screen images for all of the plays.

#+BEGIN_SRC stylus :noweb-ref full_screens
+backdrops(
	'Ant', 'AYL', 'Cor', 'Ham', 'TGV', 'WT', 'Wiv',
	'MND', 'JC', '1H4', '2H4', '1H6', '3H6', 'Jn',
	'Rom', 'Shr', 'Tmp', 'Tro')
	background-position center 0
	background-size cover
#+END_SRC

/Antony & Cleopatra/:
#+BEGIN_SRC stylus :noweb-ref full_screens
+play-backdrop('Ant', 'Tadema-Ant-The_Meeting')
	background-position 69% 25%
#+END_SRC

/As You Like It/:
#+BEGIN_SRC stylus :noweb-ref full_screens
+play-backdrop('AYL', 'Hodges-AYL-2.1')
	background-position 65% 80%
#+END_SRC

/Coriolanus/:
#+BEGIN_SRC stylus :noweb-ref full_screens
play-backdrop('Cor', 'Poussin-Cor')
#+END_SRC

/Hamlet/:
#+BEGIN_SRC stylus :noweb-ref full_screens
play-backdrop('Ham', 'Abbey-Ham-3.2')
#+END_SRC

#+BEGIN_TBD
THE FRAME is kind of an issue in this one.
#+END_TBD
/Two Gentlemen of Verona/:
#+BEGIN_SRC stylus :noweb-ref full_screens
+play-backdrop('TGV', 'Hunt-TGV-5.4-frame')
	background-position 63% 35%
	background-position 50% 35%
	// dealing with frame, but this is bad for handset portrait
	background-size calc(100% + 300px)
#+END_SRC

/The Winter's Tale/:
#+BEGIN_SRC stylus :noweb-ref full_screens
+play-backdrop('WT', 'Simon-Opie-WT-2.3')
	background-position 50% 37%
#+END_SRC

/The Merry Wives of Windsor/:
#+BEGIN_SRC stylus :noweb-ref full_screens
+play-backdrop('Wiv', 'Cruikshank-Wiv-5.5')
	background-position 50% 80%
#+END_SRC

/A Midsummer Night's Dream/:
#+BEGIN_SRC stylus :noweb-ref full_screens
+play-backdrop('MND', 'Paton-MND-2.1-Quarrel')
	background-position 43% 30%
#+END_SRC

/Julius Caesar/:
#+BEGIN_SRC stylus :noweb-ref full_screens
+play-backdrop('JC', 'Folger-JC')
	background-position 10% 0
#+END_SRC

#+BEGIN_TBD
NEED LARGER IMAGE
#+END_TBD
/Henry IV, Part 1/:
#+BEGIN_SRC stylus :noweb-ref full_screens
+play-backdrop('1H4', 'Grutzner-1H4-Boars_Head')
	background-position 50% 20%
#+END_SRC

/Henry IV, Part 2/:

This is more in the spirit of part 2 , but I don't have a good one particular to
H2.
#+BEGIN_SRC stylus :noweb-ref full_screens
play-backdrop('2H4', 'Folger-1H4')
#+END_SRC

/Henry VI, Part 1/:
#+BEGIN_NOTE
An alternate would be =Pettie-1H6=, but it's grayscale and smaller
#+END_NOTE
#+BEGIN_SRC stylus :noweb-ref full_screens
+play-backdrop('1H6', 'Payne-Roses-2')
	background-position 75% 15%
#+END_SRC

/Henry VI, Part 3/:
#+BEGIN_SRC stylus :noweb-ref full_screens
play-backdrop('3H6', 'Atkinson-3H6-2.3')
#+END_SRC

/King John/:
#+BEGIN_SRC stylus :noweb-ref full_screens
+play-backdrop('Jn', 'Unknown-Jn')
	background-position 50% 75%
#+END_SRC

/Romeo & Juliet/:
#+BEGIN_SRC stylus :noweb-ref full_screens
+play-backdrop('Rom', 'Leighton-Rom-5.3')
	background-position 64% 50%
#+END_SRC

/The Taming of the Shrew/:
#+BEGIN_SRC stylus :noweb-ref full_screens
+play-backdrop('Shr', 'Leslie-Shr-4.3')
	background-position 20% 40%
#+END_SRC

/The Tempest/:
#+BEGIN_SRC stylus :noweb-ref full_screens
+play-backdrop('Tmp', 'Romney-Tmp-1.1')
	background-position 90% 25%
#+END_SRC

/Troilus & Cressida/:
#+BEGIN_SRC stylus :noweb-ref full_screens
+play-backdrop('Tro', 'Kauffmann-Tro-5.2')
	background-position center 25%
#+END_SRC

*** BUG some images need to be cropped.

Some images show the paper from which they were scanned, meaning that a white
border is present.  You never want to see that paper, if it's at all avoidable:
it breaks the illusion.  It's especially problematic for the [[file:maps_of_the_plays.org][play map]] because
the scene links aren't readable if the background is too light.  While it might
be possible to address these with offsets here, the proper fix would be to crop
the actual background images.  This applies on a several images:

- Troilus and Cressida
- 1 Henry IV
- The Winter's Tale

** portraits

#+BEGIN_SRC dot :tangle published/dot/portrait-backdrop.dot
graph {
	layout = neato
	node [
		shape = box
		width = 2
		fixedsize = true
		pos = "0, 0!"]
	view [width = 2  style = dotted label = ""]
	image [width = .75  style = filled]
}
#+END_SRC

[[../published/images/portrait-backdrop.svg]]

The portraits are horizontally centered.

#+BEGIN_SRC stylus :noweb-ref portraits
+backdrops(
	'AWW', 'Per', 'Ado', 'MV',
	'R2', 'H8', 'Tim', 'Tit')
	background-size auto
	background-position center top
#+END_SRC

/Much Ado About Nothing/:
#+BEGIN_SRC stylus :noweb-ref portraits
+play-backdrop('Ado', 'Scheiner-Ado')
	background-position 50% 25%
#+END_SRC
If I had a better version of =Terry-Irving-Ado=, I'd use it with the Scheiner as a
diptych.  Even with =Gielgud-Leighton-1959-Ado= it's not so bad (at 40/60).  But
using a photograph breaks the spell.

/All's Well That Ends Well/:
#+BEGIN_SRC stylus :noweb-ref portraits
+play-backdrop('AWW', 'Scheiner-AWW')
	background-position 50% 50%
#+END_SRC

#+BEGIN_TBD
HACK: This uses knowledge of the shipped image width
#+END_TBD
/Pericles/:
#+BEGIN_SRC stylus :noweb-ref portraits
+play-backdrop('Per', 'Scheiner-Per')
	@media screen and (max-width 606px)
		background-position 75% 40%
#+END_SRC

/The Merchant of Venice/:
#+BEGIN_SRC stylus :noweb-ref portraits
+play-backdrop('MV', 'Gottlieb-Shylock-Jessica')
	background-position 50% 20%
#+END_SRC
You miss out on the key this way, but otherwise I find it more immersive than
the =background-size auto 150%= that shows more of the picture.  That said, this
is subject to the scale of the physical image, so in that sense I'd rather be
more explicit.

/Richard II/:
#+BEGIN_NOTE
- alternate =Terry-Katherine=, but it's too small.
- alternate =Thew-H8-4.2=, but it's too small
- variant =Blake-Fuseli-H8-4.2=
#+END_NOTE
#+BEGIN_SRC stylus :noweb-ref portraits
+play-backdrop('R2', 'Westminister-Richard_II')
	background-size auto 100%
#+END_SRC

#+BEGIN_TBD
HACK: This uses knowledge of the shipped image width
#+END_TBD
/Henry VIII/:
#+BEGIN_SRC stylus :noweb-ref portraits
+play-backdrop('H8', 'Blake-H8-4.2')
	background-position center bottom
	@media screen and (max-width 750px)
		background-position center center
		background-size 100%
#+END_SRC

/Timon of Athens/:
#+BEGIN_SRC stylus :noweb-ref portraits
+play-backdrop('Tim', 'Cosway-Timon')
	background-size auto 100%
#+END_SRC

/Titus Andronicus/:

Was using =Loutherbourg-Tit-2.3= for a while, as it is less unpleasant.  Yet this
is what one thinks of.
#+BEGIN_SRC stylus :noweb-ref portraits
+play-backdrop('Tit', 'Gravelot-Tit-3.2')
	background-position 50% 50%
#+END_SRC

** banners

These are mostly the same as full-screen, but more prone to being naturally
shorter than the viewport, in which case =cover= would scale up unacceptably.

#+BEGIN_SRC dot :tangle published/dot/banner-backdrop.dot
graph {
	layout = neato
	node [
		shape = box
		width = 2
		fixedsize = true
		pos = "0, 0!"]
	view [height = 1.5  style = dotted label = ""]
	image [height = 1  style = filled]
}
#+END_SRC

[[../published/images/banner-backdrop.svg]]

#+BEGIN_SRC stylus :noweb-ref banners
+backdrops(Lr, Cym, R3)
	background-position 50%
#+END_SRC

/King Lear/:
#+BEGIN_NOTE
HACK: This gorgeous Abbey painting works almost any way that you use it, and we
have a generously large version of it.  However, at the moment, the version that
ships is scaled to a max width of 1920, resulting in a height of 801.  I'm
baking this in here to avoid scaling it up.  That would not be an issue if we
used the “letterbox” approach, which also looks good.  But this allows us to
treat it as effectively a full-screen image on most devices.
#+END_NOTE
#+BEGIN_SRC stylus :noweb-ref banners
+play-backdrop('Lr', 'Abbey-Lr-1.1')
	background-size cover
	@media screen and (min-height 801px)
		background-size auto
#+END_SRC

/Cymbeline/:
#+BEGIN_NOTE
HACK: As with the Abbey above, we have a larger version of this image.
#+END_NOTE
#+BEGIN_SRC stylus :noweb-ref banners
+play-backdrop('Cym', 'Smetham-Cym-4.2')
	background-size cover
	@media screen and (min-height 709px)
		background-size auto
#+END_SRC

/Richard III/:
#+BEGIN_NOTE
HACK: Same as two above, except that this is in fact the full available height,
and a little upscaling may be needed even to preserve the letterbox.
#+END_NOTE
#+BEGIN_SRC stylus :noweb-ref banners
+play-backdrop('R3', 'Abbey-R3')
	background-size cover
	@media screen and (min-height 690px)
		background-size 100%
#+END_SRC

** diptychs

*** balanced diptychs

#+BEGIN_SRC stylus :noweb-ref diptychs
.background.play-TN
.background.play-Oth
.background.play-Err
	background-size 50%, 50%
	@media screen and (max-aspect-ratio 3/2)
		background-size 100%, 0%

// A little milder than some of the larger options
play-backdrop('Oth', 'Chassériau-Oth-4.3-2', 'Aldridge-Othello')
play-backdrop('Err', 'McLoughlin-Err', 'Robson-Crane-Err')
play-backdrop('TN', 'Pickersgill-Viola-Orsino', 'Pickersgill-TN-Viola-Olivia')
#+END_SRC

*** weighted diptychs

#+BEGIN_SRC stylus :noweb-ref diptychs
.background.play-Mac
.background.play-MM
	background-size 34%, 66%
	
+play-backdrop('Mac', 'Sargent-Terry-Lady_Macbeth', 'Welles-Macbeth')
	// HACK: using knowledge of shipped image size
	@media screen and (max-width 535px * 1.2)
		background-size 100%, 0%

+play-backdrop('MM', 'Scheiner-MM', 'Millais-MM-Mariana')
	// HACK: using knowledge of shipped image size... and not very well.  This
	// still allows odd gaps.  The Scheiner is 608 x 912.
	@media screen and (max-width 608px * 1.2)
		background-size 100%, 0%
#+END_SRC

** MISSING OR INADEQUATE images

#+BEGIN_SRC stylus :noweb-ref missing
// LLL
// H5


// NEED LARGER IMAGE.  This is essentially unusable as it is.
+play-backdrop('2H6', 'Abbey-Eleanor')
	background-size auto
	background-position 50%
#+END_SRC

*** Love's Labour's Lost

It would appear that Flickr search results never end.  Here are some of the
images that I like.  None are uploadable to Commons (i.e. have at least some
rights reserved), but some are open for non-commercial use.

All Rights Reserved
- http://www.flickr.com/photos/rogerchoover/3327178497/
- http://www.flickr.com/photos/70207652@N00/3282142715/
- http://www.flickr.com/photos/62552023@N06/5739946789/
- http://www.flickr.com/photos/62552023@N06/5739786093/
- http://www.flickr.com/photos/70207652@N00/3281759177/
- http://www.flickr.com/photos/70207652@N00/3282599940/
- http://www.flickr.com/photos/10088890@N05/2387066017/
- http://www.flickr.com/photos/marshin13/7410536332/

Some rights reserved
- http://www.flickr.com/photos/nooccar/8002268820/
- http://www.flickr.com/photos/umtad/4642224461/
- http://www.flickr.com/photos/umtad/4642224127/

Not sure if they're a stable addresses, but these are the “original size” links for the latter two above:
https://farm4.staticflickr.com/3355/4642224461_ced2cbd8a4_o_d.jpg
https://farm5.staticflickr.com/4065/4642224127_c649c2699f_o_d.jpg

They are from the [[https://www.flickr.com/photos/umtad/sets/72157624142478852][album]] of a 1963 production under the photo stream of
“University of Minnesota Theatre Arts & Dance”:
#+BEGIN_QUOTE
"Love's Labour's Lost" by William Shakespeare, directed by Robert D. Moulton,
presented January 31 - February 10, 1963.
#+END_QUOTE

*** Henry V

- 
   https://commons.wikimedia.org/wiki/File:Henry_V_Act_III_Scene_i.jpg (almost
  big enough for a backdrop)
- https://commons.wikimedia.org/wiki/File:Actor_Lewis_Waller_as_Henry_V.jpg

maybe Shakespearean:
-
  https://commons.wikimedia.org/wiki/File:King_Henry_V_at_the_Battle_of_Agincourt,_1415.png
 (Gilbert)
- https://commons.wikimedia.org/wiki/File:Henry_V_King.jpg

Not Shakespearean:
- https://commons.wikimedia.org/wiki/File:Henry_V_of_England.jpg
-
  https://commons.wikimedia.org/wiki/File:Henry_V_of_England_with_military_attendants_under_their_appropriate_banners.png
 (extracted from an archive.org book)
- https://commons.wikimedia.org/wiki/File:King_Henry_V_from_NPG.jpg (NPG, the
  one I've seen others use)
-
  https://commons.wikimedia.org/wiki/File:A_Chronicle_of_England_-_Page_373_-_Marriage_of_Henry_V_and_Katherine_of_France.jpg
 (but pretty, and part of a set from this book)
- https://commons.wikimedia.org/wiki/File:Marriage_of_henry_and_Catherine.jpg
  (also pretty, and old, but too small for a backdrop)
-
  https://commons.wikimedia.org/wiki/File:Le_mariage_d'Henri_V_et_de_Catherine_de_France_en_1420.jpg
 (same, but it's almost too small to make out at all)

All rights reserved, and none nearly good for this purpose, anyway.
- http://www.flickr.com/photos/stratfest/6260465213/
- http://www.flickr.com/photos/10088890@N05/2387895236/
- http://www.flickr.com/photos/lanterntheater/8567518264/
- http://www.flickr.com/photos/stratfest/7537599224/
- http://www.flickr.com/photos/44042852@N04/12938800013/
- http://www.flickr.com/photos/44042852@N04/12938799953/
- http://www.flickr.com/photos/librarytheatre/13086139233/
- http://www.flickr.com/photos/alexlupien/2417146089/
- http://www.flickr.com/photos/peterquinn5/7029748725/
- http://www.flickr.com/photos/westminster-archives/8369422463/

* image processing

Shrink the images designated as fullscreen.

#+BEGIN_SRC sh :tangle program/images/fullscreen
in="$1"
out="$2"
convert "$in" -resize '1900>' -format jpg "$out"
#+END_SRC

Now, as explained in [[file:images.org::*projections][projections]], to request that an image be provided in "full
screen" size, you simply touch the file
#+BEGIN_EXAMPLE
image-projections/{image}@fullscreen.jpg.txt
#+END_EXAMPLE
Where ={image}= is the name of the image.  ([[file:images.org::*naming%20images][Identifier]], whatever.)  That will put
the scaled image into =site/static/images/{image}@fullscreen.jpg=.

* roadmap

** TODO say something about the play feature image(s) in the entryway

Just as in the [[file:the_exhibit.org::*the%20entryway][main entryway]], you can use the otherwise empty space at the end
of the "hallway" to identify the background image.
** TODO don't show play background until it's loaded

This can be done with script, I assume.  The trick would be leaving the
non-script version intact (i.e., don't do it by removing the background styles
and then requiring script to set them).

But show something in the meantime.  And you can presumably test this with
"throttling" tools.
