#+TITLE:points development
#+PROPERTY: subgraph media_subsystem
#+PROPERTY: builds-on points

Counterpart to audio development.

I used to have a way to highlight a passage in an epub (in [[file:the_reader.org][the reader]]) and
create a "point" from it based on an auto-filled form.

* old code
** stylesheet

#+BEGIN_SRC stylus
.post-point-box
	position fixed
	top 1em
	left 20%
	right 20%
	bottom 1em
	min-height 20em
	padding 1em
	background #FFF
	border .25em solid #FF7
	box-shadow 1px 1px 3px #333
	z-index 999
	display none

	&.open
		display block

.post-point-form
	> label
	> input
	> textarea
		width 90%
		display block

	> textarea
		min-height 5em

	> input
	> textarea
		border 1px solid #CCC
		margin-bottom .5em

	input[type="button"]
	input[type="submit"]
		display inline-block
		margin-top 1em
		margin-right 1em
		width auto
#+END_SRC

** script

#+BEGIN_SRC javascript
define(['jquery'], ($) => {
	console.log("dev: initializing dev mode");
	
	$('head').append($('<link rel="stylesheet" type="text/css" href="/static/style/dev.css"/>'));
	
	// Debugging tools
	(function() {
		return;
		// Mostly for mobile, pipe console to the top of the screen.
		var _consoleLog = console.log,
			$debugEle = $('<div class="debug-window" />')
				.appendTo('body');
		
		console.log = function() {
			$debugEle.show().text([].slice.call(arguments).join(' '));
			_consoleLog.apply(console, arguments);
		};
	}());
	
	
	// Post points
	(function() {
		var resourceSection = /\/resources\/([^\/]+)\/([^\/?]+)/;
		
		var $postPointBox =
				$("<div id='post-point-box' class='post-point-box' />")
				.append($("<form id='post-point-form' class='post-point-form' method='POST' action='/dev/post-point' />")
						.append("<label>resource</label>")
						.append("<input id='resource-input' name='resource' />")
						.append("<label>section</label>")
						.append("<input id='section-input' name='section' />")
						.append("<label>anchor</label>")
						.append("<input id='anchor-input' name='anchor' />")
						.append("<label>markup</label>")
						.append("<textarea id='markup-input' name='markup' />")
						.append("<label>tags</label>")
						.append("<textarea id='tags-input' name='tags' />")
						.append("<input type='submit' value='submit' />")
						.append($("<input type='button' value='close' />")
								.on('click', function() {
									$('#post-point-box').removeClass('open');
								}))
						.on('submit', function() {
							var $form = $(this);
							$.post($form.attr('action'), $form.serialize(),
								   function() {
									   $('#post-point-box').removeClass('open');
								   });
							
							return false;
						})
					   );
		
		window.setTimeout(function() {
			// Add point post form, presumably after above stylesheet has loaded
			// (otherwise you see flicker)
			$('body').append($postPointBox);
		}, 500);
		
		function process_selection() {
			var selection = window.getSelection();
			
			if (selection.rangeCount < 1) {
				return;
			}
			
			var range = selection.getRangeAt(0),
				start = range.startContainer,
				anchor = document.evaluate(
					'(preceding::*[@id]|ancestor::*[@id])[last()]', start, null,
					XPathResult.FIRST_ORDERED_NODE_TYPE , null)
					.singleNodeValue.id,
				contents = range.cloneContents(),
				div = document.createElement("div");
			
			div.appendChild(contents);
			var markup = div.innerHTML;
			
			if (!markup) {
				return;
			}
			
			if (resourceSection.test(window.location.pathname)) {
				var resource = decodeURIComponent(RegExp.$1),
					section = decodeURIComponent(RegExp.$2);
				
				$('#resource-input').val(resource);
				$('#section-input').val(section);
				$('#anchor-input').val(anchor);
				$('#markup-input').val(markup);
				$('#post-point-box').addClass('open');
				$('#tags-input').focus().val('').val('plays/');
			}
		}
		
		// Currently doesn't support selection by keyboard
		$(document).on('mouseup', process_selection);
		
	}());
});
#+END_SRC
