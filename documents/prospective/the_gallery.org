#+TITLE:the gallery
#+PROPERTY: subgraph media_subsystem
#+PROPERTY: builds-on images

#+BEGIN_TBD
This document will be a monster to load as is, because Org will export all of
these links as inline images.
#+END_TBD

Provisionally, this is for notes about willshake's /actual/ collection of images,
as opposed to [[file:images.org][images]], which is about the general image (file) collection
process.

* code

This was the =render-figure= transform, which is still referenced in a couple of
places, but only by inactive code.  The bit where annotations are placed may
still be useful, especially to the "Procession" business above.

#+BEGIN_SRC xsl
<xsl:import href="get-href.xsl" />
<xsl:import href="render-play-quote.xsl" />
<xsl:output omit-xml-declaration="yes" />
<xsl:param name="image-key" />
<xsl:param name="class" />

<!-- Input: image element (from image index).  No default behavior -->

<xsl:template match="/image-index">
	<xsl:apply-templates mode="render-figure" select="image[@key=$image-key]" />
</xsl:template>

<xsl:variable name="quotes"
							select="document('/static/doc/quotes.xml') /quotes /quote"/>


<!-- mode: render-figure -->
<xsl:template mode="render-figure" match="node()|@*" />
<xsl:template mode="render-figure" match="image">
	<xsl:param name="class" select="$class" />
	<xsl:param name="size" select="'full'" />

	<!-- This is temp anyway because I'll pre-scale them to jpg and under static -->
	<xsl:variable name="image-file" select="concat(@key, '.jpg')"/>

	<figure data-image="{@key}" class="figure {$class}">

		<div class="figure-image-box">
			<xsl:apply-templates mode="render-figure" select="map" />

			<img src="/static/images/{$size}/{$image-file}"
					 class="figure-image"
					 alt="{caption}{self::*[not(caption)]/@key}"
					 />
		</div>
		
		<xsl:if test="caption">
			<figcaption>
				<xsl:copy-of select="caption/node()" />
			</figcaption>
		</xsl:if>
		
	</figure>
</xsl:template>
#+END_SRC

* roadmap

** TODO Selous illustrations from Cowden Clark edition

Look here:

https://archive.org/details/playseditedannot03shakuoft

https://archive.org/search.php?query=cowden%20clarke%20shakespeare

High-quality scans of this illustrated edition.  Much better than the Howards,
but still complete works and on parchment.

* leads

For Tmp, mostly Ferdinand and Miranda
- https://commons.wikimedia.org/wiki/File:Souvenir_programme_of_Shakespear's_comedy,_%22The_tempest%22_%281890%29_%2814759880046%29.jpg
- https://commons.wikimedia.org/wiki/File:Wright_tempest.jpg
- https://commons.wikimedia.org/wiki/File:The_tempest_-_a_comedy_%281901%29_%2814778908685%29.jpg
- 
   https://commons.wikimedia.org/wiki/File:The_tempest_-_a_comedy_%281901%29_%2814778691072%29.jpg
- https://commons.wikimedia.org/wiki/File:Theatrical_and_circus_life;_%281893%29_%2814765857622%29.jpg

for MV, a new upload from an archive book.  unfortunately blurry
- https://commons.wikimedia.org/wiki/File:Shakespeare's_comedy_of_the_Merchant_of_Venice_%281914%29_%2814784951443%29.jpg

Nellie Melba as Ophelie in Thomas's "Hamlet", ca. 1889-1890 - photographer Benque, Paris
https://commons.wikimedia.org/wiki/File:Nellie_Melba_as_Ophelie_in_Thomas's_%22Hamlet%22,_ca._1889-1890_-_photographer_Benque,_Paris_%284573465385%29.jpg

http://en.wahooart.com/Art.nsf/Art_EN?Open&Query=stothard,shakespeare


for AYL, see also commons category
=Wie_es_euch_gefällt_(Schlossparktheater_Berlin-Steglitz)=
	
A lot of these are on the commons, but not all?  Some of them look like the
upload was aborted.
			
https://archive.org/details/metropolitanmuseumofart-gallery?and[]=shakespeare
			
Re Ellen Terry:
http://preraphaelitesisterhood.com/dameellenterry/

Doesn't have any larger versions, but has at least one of the Lady Macbeth role
that I haven't seen.
			
See also
			
- https://www.pinterest.com/pinhjt/ellen-terry-quintessential-art-nouveau-british-sta/
- https://www.pinterest.com/hatastic/ellen-terry-theatrical-headdresses/
- https://clamorousvoice.wordpress.com/tag/ellen-terry/page/2/
- http://thetextileblog.blogspot.com/2013/12/the-theatrical-costumes-of-ellen-terry.html
- https://www.pinterest.com/pin/58335757649081469/
- http://www.vam.ac.uk/content/articles/t/the-actor-and-the-maker-ellen-terry-and-alice-comyns-carr/
			
Related to 2H6?
https://commons.wikimedia.org/wiki/File:A_Chronicle_of_England_-_Page_392_-_The_Duchess_of_Gloucester_Does_Penance.jpg
			
see also
http://spitalfieldslife.com/2011/06/24/a-door-in-cornhill/
			
TODO: Add this to the commons (already downloaded to local/2015)
http://parashutov.livejournal.com/36733.html?thread=142973

George Clint (British, 1770–1854) The Duel between Sir Toby and Sebastian from William Shakespeare's "Twelfth Night" (Act IV, Scene II). 1833.
			
Good picture, but I'm not sure what makes it Viola:
https://commons.wikimedia.org/wiki/File:Viola_in_Shakespeare%27s_Twelfth_night_Wellcome_L0074554.jpg
			
Georgian Othello production 1933.  These are really cool but unfortunately
small:
http://commons.wikimedia.org/wiki/Category:Petre_Otskheli,_Othello_1933

See also for JC, except not small.  Maybe a diptych?
- https://commons.wikimedia.org/wiki/File:1974_Shakespeare_%E2%80%93_Julius_Caesar_%281%29.jpg
- https://commons.wikimedia.org/wiki/File:1974_Shakespeare_%E2%80%93_Julius_Caesar_%282%29.jpg
			
new images for H5:
- http://commons.wikimedia.org/wiki/File:Ancient_Pistol_with_Henry_V.jpg
- http://commons.wikimedia.org/wiki/File:Fluellen_forces_Pistol_to_eat_a_leek..jpg
- http://commons.wikimedia.org/wiki/File:Mistress_Quickly,_Nym_and_Pistol.jpg
- http://commons.wikimedia.org/wiki/File:Fluellen_terrifying_Pistol.jpg

That last one is pretty good.
			
*That* Bolingbroke?
			
=George_Romney_-_Bolingbroke_and_Head_of_a_Fiend_-_Google_Art_Project.jpg=
http://commons.wikimedia.org/wiki/File:The_Duke_of_Clarence_is_assailed_by_demons_and_taunted_by_a_Wellcome_V0025889ER.jpg
			
New for MND
http://commons.wikimedia.org/wiki/File:Sir_Edward_J._Poynter_-_Helena_and_Hermia_-_Google_Art_Project.jpg
https://commons.wikimedia.org/wiki/File:A_Midsummer_Night%27s_Dream_1935.JPG
		
Is this Nerissa from MV?  Same artist also did a Cleopatra
http://commons.wikimedia.org/wiki/File:Godward-Nerissa-1906.jpg

There are a bunch of images a Bonham's.  Not sure about licensing, but at least
some of them are already on the Commons.  This includes more of thos really
lovely Rackham's:
- http://www.bonhams.com/auctions/11963/lot/166/
- http://www.bonhams.com/auctions/20486/lot/52/
- http://www.bonhams.com/auctions/21742/lot/7/
- http://www.bonhams.com/auctions/10765/lot/86/
- http://www.bonhams.com/auctions/17782/lot/1076/
- http://www.bonhams.com/auctions/20235/lot/132/
- http://www.bonhams.com/auctions/20646/lot/32/

is this a tanner?
http://commons.wikimedia.org/wiki/File:Orbis_Pictus;_K%C3%BCrschner_und_Beutler,_ca._1830_%282%29.jpg
color version
http://commons.wikimedia.org/wiki/File:Orbis_Pictus;_K%C3%BCrschner_und_S%C3%A4ckler,_ca._1830.jpg
			
See AYL on commons, Smirke's “Seven ages” series has been added, e.g.
http://commons.wikimedia.org/wiki/File:Robert_Smirke_-_The_Seven_Ages_of_Man-_Second_Childishness,_%27As_You_Like_It,%27_II,_vii_-_Google_Art_Project.jpg

The images are low-contrast, though... look into proper techniques for
retouching for color and lighting.

MOMA collection, halfheartedly licensed.
http://www.metmuseum.org/research/image-resources/frequently-asked-questions

Note the comment:
https://news.ycombinator.com/item?id=7782094

They do limit the quality of images. If you want to see the highest resolution
image, take any of the image urls and replace 'web-<WHATEVER>' with
'web-original'.

a production shot from Pacific Rep ShrewPetruchio.jpg

Is this Boydell on the commons?  It's light on h4
http://shakespeare.emory.edu/illustrated_showimage.cfm?imageid=324

another Judith, from Schajer
http://1.bp.blogspot.com/-svVgf4zTFuI/URkbHXFnWSI/AAAAAAAAEa0/NGEa3gPLucM/s1600/JudithShakespeare2.jpg
not on the Commons, that I can see

ARR, but I like this one
http://www.flickr.com/photos/14168877@N04/7825308916/

Blake illustration for Mac, not transfered to commons yet pending copyright check
https://en.wikipedia.org/wiki/File:Pity.jpg

more:
http://theshakespeareblog.com/wp-content/uploads/2014/01/Stothard-N01830_10-1024x284.jpg
http://theshakespeareblog.com/2014/01/picturing-shakespeares-characters/
which i found from here, but all pages link to "theshakespeareblog"
http://commons.earlymodernweb.org/searchcat?s=Shakespeare+on+Stage

and see also the "last post" mentioned, and its links
http://theshakespeareblog.com/2014/01/taking-to-the-streets-with-shakespearescharacters/
http://luna.folger.edu/luna/servlet/detail/FOLGERCM1~6~6~334643~144330:Shakspearian-Jubilee-Procession,-18#

the latter having a CC-NC license.  i exported the largest version allowed (about 1500px wide)

http://www.bbc.co.uk/arts/yourpaintings/paintings/the-apotheosis-of-garrick-54891

there's also this (Gilbert's “Apotheosis of Shakespeare’s Characters”), but all
versions I've seen so far are bad and/or watermarked:

http://www.1st-art-gallery.com/Sir-John-Gilbert/Apotheosis-Of-Shakespeares-Characters.html
http://www.allposters.com/-sp/Apotheosis-of-Shakespeare-s-Characters-1871-Posters_i10072445_.htm

and then there's this Rubens on Henry IV:
http://artunframed.com/Gallery/shop/the-apotheosis-of-henry-iv-and-the-proclamation-of-the-regency-of-marie-de-medici-on-the-14th-of-may-1610/#prettyPhoto/0/


check out

#+BEGIN_EXAMPLE
John_Hamilton_Mortimer_-_Literary_Characters_Assembled_Around_the_Medallion_of_Shakespeare_-_Google_Art_Project.jpg
George_Cruikshank_-_The_First_Appearance_of_William_Shakespeare_on_the_Stage_of_the_Globe_Theatre_-_Google_Art_Project.jpg
Brown-The_Seeds_and_Fruits_of_English_Poetry.jpg
#+END_EXAMPLE

TODO: the titania from this page is not in the commons

http://19thcenturyusapaint.blogspot.com/search?q=abbey

	
a bunch of AYL images from Tulane's Dixon hall but not so useful
	
so there's this cartoon version of lear starting with King-lear-cartoon1.jpg but
I don't see it as very educational (so?)

- https://commons.wikimedia.org/wiki/File:Lady_Macbeth_of_the_Mtsensk_District.jpg
- https://commons.wikimedia.org/wiki/File:A_chronicle_history_of_the_life_and_work_of_William_Shakespeare,_player,_poet,_and_playmaker_%281886%29_%2814580213138%29.jpg
- https://commons.wikimedia.org/wiki/File:Parkinglot.jpg
- https://commons.wikimedia.org/wiki/File:The_comedies,_histories,_tragedies,_and_poems_of_William_Shakspere_%281851%29_%2814761468376%29.jpg
- https://commons.wikimedia.org/wiki/File:The_views_about_Hamlet,_and_other_essays_%281906%29_%2814803769943%29.jpg

A nice sketch of the proscenium theater:
https://commons.wikimedia.org/wiki/File:An_introduction_to_Shakespeare_%281910%29_%2814596458809%29.jpg
Not sure the original source, though.

PDF's of "All the Year Round," supposedly with some Shakespeare illustrations.
- https://commons.wikimedia.org/wiki/File:All_the_Year_Round_-_Series_2_-_Volume_25.pdf
-
  https://commons.wikimedia.org/wiki/File:All_the_Year_Round_-_Series_2_-_Volume_6.pdf
- and [[https://commons.wikimedia.org/w/index.php?title%3DSpecial:Search&limit%3D100&offset%3D0&profile%3Ddefault&search%3Dcolumbia%2Bshakespeare][more]]
I didn't realize that "Boz" was also a Dickens.

The other Welles Mac has been taken down (hopefully not forever); in the
meantime, there's this:
https://commons.wikimedia.org/wiki/File:Orson_Welles_as_Macbeth.jpg

https://commons.wikimedia.org/wiki/File:Leslie_Howard_1937_CBS.jpg
"Photo of Leslie Howard performing in a radio presentation of Shakespeare's Much Ado About Nothing."

Klimt painting, someone scanned from a book.  Interesting, first one I can think
of that's of /people watching/ Shakespeare
https://commons.wikimedia.org/wiki/File:Klimt_-_Theater_Shakespeares.jpg

Wow, nice venue, Oregon Shakespeare Festival
https://commons.wikimedia.org/wiki/File:Elizabethan_Theatre_2014.jpg

Globe panorama, get a tripod, already
https://commons.wikimedia.org/wiki/File:The_Globe_Theatre,_Panorama_Innenraum,_London.jpg

Part of a series of I guess set designs?  Really beautiful
https://commons.wikimedia.org/wiki/File:Kirovabad_Theater_1933_Shakespeare_%E2%80%93_Othello_%283%29.jpg

Part of a series I hadn't noticed before.  Grainy B&W stage shots of some German Hamlet:
https://commons.wikimedia.org/wiki/File:Fotothek_df_pk_0000036_014_Horst_Caspar_in_der_Titelrolle,_Walter_Richter_als_Claudius,_Gerda_M%C3%BCller_als_Ge.jpg

Sketch of MND, looks like Puck knocking the lady off her stool
https://commons.wikimedia.org/wiki/File:Shakespeare's_comedy_of_A_midsummer-night's_dream_%281914%29_%2814566184678%29.jpg

Popular Electricity Magazine?  Something about the effect of creating the
ghost... still this is a very recognizable moment.
https://commons.wikimedia.org/wiki/File:Popular_electricity_magazine_in_plain_English_%281913%29_%2814765076712%29.jpg

Drawing of Herbert Beerbohm Tree as Benedick and Winifred Emery as Beatrice in a 1905 production. Act II, Scene v: "Kill Claudio".
https://commons.wikimedia.org/wiki/File:Kill_Claudio.jpg

Brutus sees Caesar's ghost
https://commons.wikimedia.org/wiki/File:Brutus_sees_Caesar's_ghost.jpg

Illustration of Priscilla Horton as "Ariel" in The Tempest, circa 1838
https://commons.wikimedia.org/wiki/File:Harvard_Theatre_Collection_-_Priscilla_Horton_TCS_45.jpg

Yet another Prospero commanding Ariel
https://commons.wikimedia.org/wiki/File:Prospero_commanding_Ariel.jpg

Lots of stuff on Google today.  Here's one I haven't seen for /Shrew/:
https://www.google.com/culturalinstitute/asset-viewer/scene-from-shakespeare-s-the-taming-of-the-shrew-katharina-and-petruchio/4AGtBDwni1Z91w?projectId=art-project

** wikimedia / archive

I've noticed recently that the Commons has images from Internet Archive books,
apparently from an automated (or mostly automated) process in which images are
marked up with their surrounding text and linked to the source page in IA's
reader.

A great number of new illustrations have become available in this way since my
first pass, and it could be of some service for me to categorize them (which is
how I found the ones that I found in the first place).  Moreover, it may mean
that the Cowden-Clarke edition (with Selous's illustrations) gets processed in
this way, before I get around to doing it myself.  Since the quality of the work
is very good (from what I've seen so far), that would be ideal.

-
  https://commons.wikimedia.org/wiki/File:The_dramatic_works_of_William_Shakespeare_-_accurately_printed_from_the_text_of_the_corrected_copy_left_by_the_late_George_Steevens,_Esq._-_with_a_glossary,_and_notes,_and_a_sketch_of_the_life_of_%2814595774429%29.jpg
-
  https://commons.wikimedia.org/wiki/File:An_art_edition_of_Shakespeare,_classified_as_comedies,_tragedies,_histories_and_sonnets,_each_part_arranged_in_chronological_order,_including_also_a_list_of_familiar_quotations_%281889%29_%2814596447207%29.jpg
- https://commons.wikimedia.org/wiki/File:Typical_tales_of_fancy,_romance,_and_history_from_Shakespeare's_plays;_in_narrative_form,_largely_in_Shakespeare's_words,_with_dialogue_passages_in_the_original_dramatic_text_%281892%29_%2814595126580%29.jpg
