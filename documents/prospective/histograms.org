#+TITLE:histograms
#+PROPERTY: invisible

A /histogram/ is a graphical display of a set of numbers.  Most people call it a
"bar graph" or a "bar chart."

As I say elsewhere, willshake is /not about counting/.  I'm just keeping this code
because it was a decent little implementation, and it actually wasn't so
offensive when used for the roles.

* the structure

The structure of a histogram (when rendered as markup) was:
#+BEGIN_EXAMPLE
ul.histogram
	li
		a
			span.bar (width based on value)
			span.text
				span.label (the item's text)
				span.sep (default to dash)
				span.value (the number and units)
					span.percentage (as percentage of total)
#+END_EXAMPLE

* old code

** transforms

This, =render-graph-items=, was a wrapper for =render-graph-items-impl=, below.

#+BEGIN_SRC xsl
<xsl:import href="render-graph-items-impl.xsl" />
<xsl:output omit-xml-declaration="yes"/>
<xsl:param name="item-plural" select="'items'"/>

<xsl:template match="node()|@*" />
<xsl:template match="/">
	<xsl:apply-templates select="." mode="render-graph-items">
		<xsl:with-param name="item-plural" select="$item-plural"/>
	</xsl:apply-templates>
</xsl:template>
#+END_SRC

This was =render-graph-items-impl.xsl=.

Its purpose was to emit list items with metadata needed to display the
histogram.

It took as input a set of elements representing a set of "data points."  The
attributes recognized were:

- item-id :: unique key
- label :: display caption
- href :: link target
- value :: scalar number for graph

It assumed the provided nodes had a wrapper element.

#+BEGIN_SRC xsl
<xsl:template match="*" mode="render-graph-items">
	<xsl:param name="item-plural" select="@item-plural"/>
<xsl:param name="limit" />

<xsl:variable name="total" select="sum(*/@value)"/>

<xsl:variable name="max-value">
<xsl:for-each select="*">
<xsl:sort select="@value" data-type="number" order="descending"/>
<xsl:if test="position()=1">
	<xsl:value-of select="@value"/>
</xsl:if>
</xsl:for-each>
</xsl:variable>

<xsl:for-each select="*">
	<xsl:sort select="@value" data-type="number" order="descending"/>
	<xsl:if test="not($limit) or position() &lt;= $limit">
		<li>
			<xsl:if test="@item-id">
				<xsl:attribute name="id">
					<xsl:value-of select="@item-id"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:copy-of select="@*[starts-with(name(), 'data-')]" />


			<a href="{@href}">
				<xsl:call-template name="graph-item-contents">
					<xsl:with-param name="value" select="@value"/>
					<xsl:with-param name="label" select="@label"/>
					<xsl:with-param name="total" select="$total"/>
					<xsl:with-param name="item-plural" select="$item-plural"/>
				</xsl:call-template>
			</a>
		</li>				
	</xsl:if>

</xsl:for-each>
</xsl:template>

<xsl:template name="graph-item-contents">
	<xsl:param name="value" />
	<xsl:param name="offset" />
	<xsl:param name="label" />
	<xsl:param name="total" />
	<xsl:param name="item-plural" />
	
	<xsl:variable name="percent-of-total" select="100 * $value div $total"/>
	
	<span class="bar">
		<xsl:attribute name="style">
			<xsl:value-of select="concat('width:', format-number($value div $total * 100, '0.##'), '%')"/>
			<xsl:if test="$offset &gt; 0">
				<xsl:value-of select="concat('; left:', format-number($offset div $total * 100, '0.##'), '%')"/>
			</xsl:if>
		</xsl:attribute>
	</span>
	<span class="text">
		<span class="label"><xsl:value-of select="$label"/></span>
		<span class="sep"><xsl:text> — </xsl:text></span>
		<span class="value">
			<xsl:value-of select="$value"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="$item-plural"/>

			<xsl:text> </xsl:text>
			<span class="percentage">
				<xsl:value-of select="round($percent-of-total)"/>
				<xsl:text>%</xsl:text>	
			</span>
		</span>
	</span>

</xsl:template>

#+END_SRC

** style

#+BEGIN_SRC stylus
@require plain_list
 
.histogram
	plain-list()
	
	> .graph-item
	> li > a
		//position relative		// allow positioning of bar
		display block			// makes all containers the same width
	
	.bar
		position absolute
		display block
		top 0
		left 0
		height 100%
		background rgba(white, .5) // NO!
		border-radius 2px
	
	.text
		// To get above bar
		position relative
		z-index 1
	
	.label
		font-weight normal
	
	// Mainly for non-stylesheet viewing
	.sep
		display none
	
	.value
		font-weight 200			// MAYBE NO!
		margin-left .5em
	
	.percentage
		margin-left .25em
		opacity .5				// MAYBE NO! or specific style
#+END_SRC
