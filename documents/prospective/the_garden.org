#+TITLE:the garden
#+PROPERTY: invisible
#+PROPERTY: subgraph the_future

I've had this idea for a long time that willshake would feature some kind of
"garden."

One of the books I came across (and collected) was /The plant-lore & garden-craft
of Shakespeare/, by Henry Nicholson Ellacombe.  I thought this could serve as a
starting point for a kind of virtual "Shakespeare garden," or a guide of some
sort.

I was hoping there was a comprehensive, authoritative, and canonical book of
botanical illustrations in the public domain, a sort of Grey's Anatomy for
plants, that just happens to be digitized in high-quality.

Well, my word, I love the Commons.  The "botanical illustrations" category has
"become too crowded" and we are asked to move images into subcategories.  The
"good images" view of this page is very striking (though not altogether
botanical).  And there are dozens of subcategories by illustrator.  So yeah.

God of creation, what wonders here.  It appears that in my first excursions I
got the wrong impression because I was searching by common name.
  
https://commons.wikimedia.org/wiki/Aconitum_napellus
  
https://commons.wikimedia.org/wiki/Category:Aconitum_napellus_-_botanical_illustrations
