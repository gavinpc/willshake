#+TITLE:points
#+PROPERTY: subgraph media_subsystem
#+PROPERTY: builds-on the_library

/Points/ are what I've called /references between works/.

* structure

The structure of a point (when rendered as HTML) was

#+BEGIN_EXAMPLE
	article.point
		blockquote.quote [.extract]
			div.quote-text [.quote-text-N-chars (where N is rounded to 100)]
				p.quote-text-p+
				a.source-link
					.source-link-text
			footer.quote-footer
				cite
					a.source-link
						span.source-link-text
		footer.from
			span.author*
				span.avatar.{person-key}?
				span.name
			a.work
				i.title
			a.work-section?
				span.title
					(.before and .after with “”)
			span.year
#+END_EXAMPLE

* style

I never quite settled on a single way of displaying points for any context, but
this was all prior to the last big redesign, when I had no conceptual and
spatial foundation for these things.  Indeed, the biggest problem was always
that there's no physical construct for free-floating blobs of (flow-positioned)
text in space.  That's the first thing I would have to decide before restoring
these.

Points are quotes and figures from resources.

A =.point= can either be a div containing figure, or a blockquote.
	
May eliminate or restructure =figure= usage.

#+BEGIN_SRC stylus
@require fonts
@require typesetting
 
.point
	margin 1em auto
	padding-bottom 1em			// prefer to margin because of collapse
	width 40em
	max-width 100%
	
	.quote
		margin-left 0			// beat quote default
		margin-right 0			// beat quote default
		margin-bottom .5em		// beat quote default
	
	.quote-text
		border-bottom-left-radius 0  // duplicated in epub
		padding .75em 1.5em
		text-align justify
		line-height 1.2
	
	.quote-text-p::first-line
		font-size 150%
		line-height .83			// offset larger font size
	
	.from
		// This is needed where point is inside a book font element.
		body-font()
	
	.avatar
		float left
		margin-bottom .5em

	/* DISABLED: this should not apply generally, may reuse in more specific
	setting.
	
	.quote-100-chars > .quote-text-p { font-size: 166%; }
	.quote-200-chars > .quote-text-p,
	.quote-300-chars > .quote-text-p { font-size: 150%; }
	.quote-400-chars > .quote-text-p { font-size: 133%; }
	.quote-500-chars > .quote-text-p { font-size: 125%; }
	.quote-600-chars > .quote-text-p { font-size: 120%; }
	.quote-1200-chars > .quote-text-p,
	.quote-1100-chars > .quote-text-p { font-size: 110%; line-height: 1.3; }
	.quote-1200-chars > .quote-text-p,
	.quote-1300-chars > .quote-text-p { font-size: 105%; line-height: 1.3;  }
	.quote-1400-chars > .quote-text-p,
	.quote-1500-chars > .quote-text-p { font-size: 100%; line-height: 1.3; }
	.quote-1600-chars > .quote-text-p,
	.quote-1700-chars > .quote-text-p,
	.quote-1800-chars > .quote-text-p,
	.quote-1900-chars > .quote-text-p,
	.quote-2000-chars > .quote-text-p { font-size: 100%; line-height: 1.3;  }
	*/
#+END_SRC

* quotes

Points were generally a special case of /quotes/.

Notes indicate that there were two high-level classes of this, for quotes from
the resources (i.e. books) and those from the plays.  Because play quotes were
displayed in a special "dialoguey" way.

Do not set font-size on .quote-text, but on quote-text-p, so that you can do
em-based layout against .quote-text.  All =.quote-text= should contain
=.quote-text-p=.

#+BEGIN_SRC stylus
@require docking
@require fonts
@require typesetting

.quote-text
	book-font()
	padding .25em .5em
	
	// TODO: this puts the quote in a different layer, i.e. it doesn't get
	// composited with parent.  That's not the intention here.
	position relative			// enable .source-link to fill container
	
	hyphens()
	
	// A "speech bubble" around the text.
	border-radius .25em
	background-image linear-gradient(hsl(0,0%,100%), hsl(0,0%,97%))
	
	color #111					// why?

.quote-text-p
	
	// As the quote box is already padded, paragraph margins are only to
	// separate paragraphs.  Single-paragraph quotes should have none.
	margin-top .5em				// beat p default
	margin-bottom 0					// beat p default
	&:first-child
		margin-top 0

.quote
	.work
		color inherit			// beat link default
		
		> .title
			font-style italic	// already using 'i' element, but for clarity
	
	// Touching the quote anywhere should take you to the source.
	//
	// This creates an invisible link that covers the entire quote area.
	.source-link:after
		hug-parent()
		content ''
	
	// The default text is for non-stylesheet viewing only.  This makes the
	// source link itself zero height.
	.source-link-text
		display none

// Play quotes: which are also .quote
// TODO: separate file?  Inherit?
.play-quote
	position relative
	
	.quote-text
		border-bottom-right-radius 0
	
	.attribution
		color white
		font-weight 200
		
		display inline-block
		margin-right .25em
		margin-bottom .25em

#+END_SRC

** selecting

This was just about quotes from the plays.

The purpose of this transform was to filter quotes data to those matching
provided tag (with special case for selecting quote for a play, since all quotes
are tagged with a play).  Output should be in same structure.

It took as input the quotes file.

#+BEGIN_SRC xsl
<xsl:param name="about"/>
<xsl:param name="limit"/>

<xsl:key name="quote-about" match="quote|exchange" use="about/@ref" />

<!-- Enforce absolute path.  This is really just to deal with root, which may
	   come in as empty. -->
<xsl:variable name="path">
	<xsl:choose>
		<xsl:when test="starts-with($about, '/')">
			<xsl:value-of select="$about"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="concat('/', $about)"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>


<xsl:variable name="play-path" select="substring-after($path, '/plays/')"/>

<xsl:template match="/quotes">
	<quotes>

		<xsl:choose>
			<!-- $limit is implemented identically for both -->
			<xsl:when test="starts-with($path, '/plays/') and not(contains($play-path, '/'))">
				<xsl:variable name="play-key" select="$play-path"/>
				<xsl:copy-of select="(quote[@play=$play-key])[not($limit) or position() &lt;= $limit]"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy-of select="key('quote-about', $path)[not($limit) or position() &lt;= $limit]" />
			</xsl:otherwise>
		</xsl:choose>

	</quotes>
</xsl:template>
#+END_SRC

* epigrams

An /epigram/ is a selected quote for a particular place, usually at the head.  A
point can serve as an epigram.

This was a shelved stylesheet, but contains some metrics for positioning the
selected quotes against the play backgrounds that I may refer back to.

#+BEGIN_SRC stylus
@require scale_type
@require paper

.epigrams
	position absolute
	left 0
	right 0
	bottom 100%
	pointer-events none // allow click through this layer, overridden for the quotes

.epigram
	position relative			// to allow positioning of quote mark
	display inline-block
	width 49%					// must be box-shadow putting it over
	transition .2s				// for font-size change
	
	.quote
		pointer-events auto		   // quotes are the only clickable area on epigrams
		
		&.play .attribution
			border-radius .5em 0 .5em .5em // override default for .play.quote, move pointed corner to right
	
	.quote-text
		text-align inherit		// beat justify from points
	
	&.works
		text-align right
		margin-bottom -1.15em // play quote attribution height a hack to bottom align to quote-text
	
	&.resources
		text-align left
		margin-bottom -4em // resources quote attribution height a hack to bottom align to quote-text
	
	.quote-text
		paper-print()
		display inline-block	// so that box is fit to content regardless of placement
		color #111
		box-shadow -.5em .5em 1em rgba(black, .5)
		border-radius .25em
		border-left-bottom-radius 0
	
	.quote-text-p
		font-size 166%
		line-height .83
	
	.point
		.avatar
		.work
		.work-section
		.year
			display none
		
		.author > .name
			font-size 100%
			display none


// dev working on this
// For bottom-aligned pages, allow positioning epigrams against viewport
// 
// This won't work right now anyway, because the -aligned classes are gone.
.bottom-aligned
	
	 .epigrams
		height 100%
		margin-bottom -8em			// var top margin of bottom-aligned page
		vertical-align bottom
	
	.epigram
		position absolute
		bottom 10em					// get back above page heading
	
		&.resources
			right 1em

// Special per-play epigram positioning (or hiding)
[data-epigrams-path="/plays/Ant"] .resources.epigram
[data-epigrams-path="/plays/WT"] .resources.epigram
	left 10%

[data-epigrams-path="/plays/Tmp"] .resources.epigram
	top 2em
	left 37%
	max-width 36%

[data-epigrams-path="/plays/Jn"] .resources.epigram
[data-epigrams-path="/plays/MV"] .resources.epigram
[data-epigrams-path="/plays/Wiv"] .resources.epigram
	left 60%
	width auto

[data-epigrams-path="/plays/Cor"] .resources.epigram
[data-epigrams-path="/plays/Jn"] .resources.epigram
	bottom 4em

[data-epigrams-path="/"] .resources.epigram
[data-epigrams-path="/plays"] .resources.epigram
	position absolute
	bottom 0
	left auto
	right 1em
	
[data-epigrams-path="/plays/AYL"] .resources.epigram
	right 1em
	bottom 40%
	text-align right
	max-width 35%

[data-epigrams-path="/plays/Rom"] .resources.epigram
	right 1em
	bottom 60%
	text-align right
	max-width 35%

// TODO: just don't emit epigrams for these
[data-epigrams-path="/"] .works
[data-epigrams-path="/plays"] .works
[data-epigrams-path="/plays/Rom"] .works
[data-epigrams-path="/plays/MND"] .works
[data-epigrams-path="/plays/R3"] .works
[data-epigrams-path="/plays/Jn"] .works
[data-epigrams-path="/plays/MV"] .works
[data-epigrams-path="/plays/WT"] .works
[data-epigrams-path="/plays/Tmp"] .works
[data-epigrams-path="/plays/Shr"] .resources
[data-epigrams-path="/plays/H8"] .resources
[data-epigrams-path="/plays/Ant"] .works
[data-epigrams-path="/plays/Wiv"] .works
[data-epigrams-path="/plays/AYL"] .works
[data-epigrams-path="/plays/TGV"] .works
[data-epigrams-path="/plays/Ham"] .works
	&.epigram
		display none

[data-epigrams-path="/plays/1H4"] .works.epigram
	margin-top $contentTop
	top 5%
	left 10%
	width 25%
	
	.quote-text-p
		font-size 110%
	
	.attribution
		display none

[data-epigrams-path="/resources"] .works.epigram
	visibility hidden

[data-epigrams-path="/plays/Mac"] .resources.epigram
[data-epigrams-path="/plays/Ham"] .resources.epigram
	left 0
	right 50%
	text-align right

[data-epigrams-path="/plays/Mac"] .epigram .avatar
[data-epigrams-path="/plays/Ham"] .epigram .avatar
	float right

[data-epigrams-path="/plays/Mac"] .works.epigram
	left 50%
	right 0
	text-align left
	bottom 6em
	max-width 20em
// end special per-play layout
#+END_SRC
* marks

=mark.ed= is used where I have added emphasis to markup (currently only in
points).

#+BEGIN_SRC stylus
@require colors
mark.ed
	font-weight bold
	font-style normal
	background inherit
	
	&.highlight
		font-weight normal
		background $highlightColor
#+END_SRC
* putting on timeline

I've gone through many iterations of trying to integrate these things with a
timeline.  Here's some code from the least-old efforts.

#+BEGIN_SRC xsl
<xsl:import href="render-point.xsl" />
<xsl:output omit-xml-declaration="yes" />

<!-- Input: points, static or selected -->

<xsl:template match="/points">
	<xsl:apply-templates select="." mode="points-time" />
</xsl:template>

<!-- mode: points-time -->
<xsl:template mode="points-time" match="node()|@*" />
<xsl:template mode="points-time" match="/points">
	<time>
		<xsl:apply-templates mode="points-time" />
	</time>
</xsl:template>

<!-- TEMP, will change date format -->
<!-- <xsl:template mode="points-time" match="point[from/quote][to[starts-with(@ref, '/year')]]"> -->
<!-- 	<xsl:variable name="ref" select="(to[starts-with(@ref, '/year')])[1]/@ref"/> -->
<!-- 	<xsl:variable name="year" select="substring-after($ref, '/year/')"/> -->
<xsl:template mode="points-time" match="point[from[quote or extract]]">
	<xsl:variable name="year" select="from/resource/dates/*[1]/@year"/>

	<item id="point-{position()}">
		<date year="{$year}" />
		<summary>
			<xsl:apply-templates select="." mode="render-point" />
		</summary>
	</item>
</xsl:template>
#+END_SRC
* select points

The purpose of this transform was to select points with the given target and
merge in resource metadata for point source. Match is case-sensitive.  It took
the "points" file as input.

#+BEGIN_SRC xsl
<xsl:param name="to"/>
<!-- Will select only from this group if one matches, otherwise ignored. -->
<xsl:param name="prefer-group" />
<xsl:param name="skip-group" />
<!-- Include paths below $to.  Ignored when prefer-group finds match. -->
<xsl:param name="deep"/>

<xsl:key name="to" match="to" use="@ref" />

<xsl:variable name="resources"
							select="document('/static/doc/resources.xml')/resource-index/resources" />

<xsl:template match="/points">
	<xsl:apply-templates mode="select-points" select="." />
</xsl:template>

<!-- mode: select-points -->

<xsl:template mode="select-points" match="node()|@*">
	<xsl:copy>
		<xsl:apply-templates mode="select-points" select="node()|@*" />
		<xsl:apply-templates mode="extended-metadata" select="." />
	</xsl:copy>
</xsl:template>

<xsl:template mode="select-points" match="points">
	<xsl:param name="to" select="$to" />
	<xsl:param name="prefer-group" select="$prefer-group" />
	<xsl:param name="deep" select="$deep" />

	<xsl:variable name="to-use">
		<!-- HACK to handle root path -->
		<xsl:if test="$to=''">/</xsl:if>
		<xsl:value-of select="$to"/>
	</xsl:variable>
	<xsl:variable name="deep-search" select="$deep='true'" />

	<xsl:variable name="matching" select="key('to', $to-use)"/>

	<points>
		<xsl:choose>
			<xsl:when test="$prefer-group">
				<xsl:variable name="preferred" select="$matching[@group=$prefer-group]"/>

				<xsl:apply-templates mode="select-points"
														 select="(($preferred
																		 |$matching[not($preferred)])
																		 /..)[1]" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates mode="select-points"
														 select="$matching
																		 [not($skip-group) or not(@group = $skip-group)]
																		 /.." />
				<xsl:if test="$deep-search">
					<xsl:apply-templates
							mode="select-points"
							select="point[
											starts-with(concat(to/@ref, '/'), $to-use)]" />
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</points>

</xsl:template>

<!-- mode: extended-metadata -->
<xsl:template mode="extended-metadata" match="node()|@*" />
<xsl:template mode="extended-metadata" match="point/from[@resource]">
	<xsl:copy-of select="$resources/resource[@key=current()/@resource]" />
</xsl:template>

#+END_SRC
* render point

Thd purpose of this transform was to define templates for formatting points with
default HTML presentation.  Does not have a root template of its own.

It takes as input =point= elements extended with resource metadata.

#+BEGIN_SRC xsl
<xsl:import href="render-avatar.xsl" />
<xsl:import href="epub-functions.xsl" />

<!-- mode: render-point -->
<xsl:template mode="render-point" match="node()|@*" />
<xsl:template mode="render-point" match="point">
	<div>
		<xsl:call-template name="point-attributes" />
		<xsl:apply-templates select="." mode="point-element" />
	</div>
</xsl:template>

<xsl:template mode="render-point" match="point[from[quote or extract]]">
	<article>
		<xsl:call-template name="point-attributes">
			<xsl:with-param name="extra-class">
				<xsl:if test="self::extract"> extract</xsl:if>
				<xsl:apply-templates select="." mode="point-extra-class" />
			</xsl:with-param>
		</xsl:call-template>
		
		<xsl:apply-templates select="." mode="point-element" />
	</article>
</xsl:template>

<!-- mode: point-extra-class -->
<xsl:template mode="point-extra-class" match="node()|@*" />
<xsl:template mode="point-extra-class" match="point[from/entire-resource]">
	<xsl:text> from-entire-resource</xsl:text>
</xsl:template>

<xsl:template mode="point-extra-class" match="point[from[@section][extract][not(quote)]]">
	<xsl:text> from-entire-resource-section</xsl:text>
</xsl:template>


<!-- mode: render-point-element -->
<xsl:template mode="point-element" match="node()|@*" />
<xsl:template mode="point-element" match="point">
	<xsl:apply-templates select="." mode="point-content" />
	<xsl:apply-templates select="." mode="point-attribution" />
</xsl:template>


<!-- mode: render-point-content -->
<xsl:template mode="point-content" match="node()|@*" />
<xsl:template mode="point-content" match="point">
	<xsl:apply-templates mode="point-content" />
</xsl:template>

<xsl:template mode="point-content" match="from[quote or extract]">
	<xsl:apply-templates mode="point-content" />
</xsl:template>

<xsl:template mode="point-content" match="quote|extract">
	<xsl:variable name="length-group" select="round(string-length(.) div 100) * 100"/>

	<xsl:variable name="source-href">
		<xsl:apply-templates select=".." mode="point-source-href" />
	</xsl:variable>

	<blockquote cite="{$source-href}" class="quote">
		<div class="quote-text quote-{$length-group}-chars">
			<xsl:apply-templates select="." mode="point-text" />

			<footer>
				<cite>
					<a href="{$source-href}" class="source-link">
						<span class="source-link-text">(source)</span>
					</a>
				</cite>
			</footer>
		</div>
	</blockquote>
</xsl:template>

<xsl:template mode="point-content" match="from[@resource][image]">
	<figure>
		<a href="/resources/{@resource}/{@section}#{image/@anchor}">
			<img src="/_resources/epubs/{@resource}/{image/@src}"
					 alt="{normalize-space(image/caption)}"
					 class="point-image" />
		</a>
	</figure>
	<figcaption>
		<xsl:copy-of select="image/caption/node()"/>
	</figcaption>
</xsl:template>

<!-- mode: render-point-text -->
<xsl:template mode="point-text" match="node()|@*">
	<xsl:copy>
		<xsl:apply-templates mode="point-text" select="node()|@*" />
	</xsl:copy>
</xsl:template>

<xsl:template mode="point-text" match="p">
	<p class="quote-text-p">
		<xsl:apply-templates mode="point-text" select="node()|@*" />
	</p>
</xsl:template>

<xsl:template mode="point-text" match="quote|extract">
	<!-- TODO: This takes whitespace from source that I don't want.  I should
		   fix source, or modify this so it is skipped. -->
	<p class="quote-text-p">
		<xsl:apply-templates mode="point-text" select="node()" />
	</p>
</xsl:template>

<xsl:template mode="point-text" match="quote[p]|extract[p]">
	<xsl:apply-templates mode="point-text" />
</xsl:template>

<!-- mode: render-point-attribution -->
<xsl:template mode="point-attribution" match="node()|@*" />
<xsl:template mode="point-attribution" match="point">
	<xsl:apply-templates mode="point-attribution" />
</xsl:template>

<xsl:template mode="point-attribution" match="point[from[quote or extract]]">
	<xsl:apply-templates mode="point-attribution" />
</xsl:template>

<xsl:template mode="point-attribution" match="from">
	<!-- Can get more than one author. -->
	<xsl:variable name="author"
								select="resource/contributors/author"/>
	
	<xsl:variable name="citation-href">
		<xsl:apply-templates select="." mode="point-source-href" />
	</xsl:variable>

	<footer class="from">
		
		<xsl:for-each select="$author">
			<span class="author">
				<xsl:call-template name="render-avatar">
					<xsl:with-param name="person-key" select="@key"/>
				</xsl:call-template>
				<span class="name">
					<xsl:value-of select="@name"/>
				</span>
			</span>
			<br />
		</xsl:for-each>
		
		<xsl:if test="@section">
			<a href="/resources/{@resource}/{@section}" class="work-section">
				<span class="title">
					<span class="before">“</span>
					<xsl:call-template name="epub-section-title" />
					<span class="after">”</span>
				</span>
			</a>
			<br />
		</xsl:if>
		
		<a class="work" href="/resources/{@resource}">
			<i class="title">
				<xsl:value-of select="resource/title"/>
			</i>
		</a>

		<xsl:if test="resource/dates/*[not(self::e-publication)]">
			
			<xsl:variable name="date" select="resource/dates/*[1]"/>
			
			<br />
			<span class="year">
				<xsl:value-of select="$date/@year"/>
			</span>

		</xsl:if>

	</footer>
	
</xsl:template>

<!-- mode: point-source-href -->
<xsl:template mode="point-source-href" match="node()|@*" />
<xsl:template mode="point-source-href" match="point/from">
	<xsl:variable name="anchor" select="*[1]/@anchor"/>

	<xsl:value-of select="concat('/resources/', @resource)" />
	<xsl:if test="@section">
		<xsl:value-of select="concat('/', @section)"/>
	</xsl:if>

	<xsl:if test="$anchor">
		<xsl:value-of select="concat('#', $anchor)"/>
	</xsl:if>
</xsl:template>

<!-- function: point-attributes -->
<xsl:template name="point-attributes">
	<xsl:param name="extra-class" />
	
	<!-- HACK: if points are being added into #time, they need a unique key to
		   be removed correctly.  This is arbitrary and won't be unique across
		   queries, but it should be good enough for now. -->
	<xsl:attribute name="data-point">
		<xsl:value-of select="generate-id()"/>
	</xsl:attribute>

	<xsl:variable name="year" select="(.//@year)[1]"/>
	<xsl:if test="$year">
		<xsl:attribute name="data-year">
			<xsl:value-of select="$year"/>
		</xsl:attribute>
		
		<xsl:attribute name="style">
			<xsl:value-of select="concat('order:', $year)"/>
		</xsl:attribute>
	</xsl:if>

	<xsl:attribute name="class">
		<xsl:text>point</xsl:text>
		<xsl:if test="$extra-class">
			<xsl:value-of select="$extra-class"/>
		</xsl:if>
	</xsl:attribute>
</xsl:template>
#+END_SRC

** render points

The purpose of this transform was to apply render-point to given points---the
kind of thing you'd never need in a real functional language.

It took as input the output from =select-points= (points with extended metadata).

#+BEGIN_SRC xsl
<xsl:import href="render-point.xsl" />

<xsl:variable name="resource-index" select="document('/static/doc/resources.xml')"/>
<xsl:variable name="resources" select="$resource-index/resource-index/resources"/>


<xsl:template match="/points">
	<xsl:apply-templates select="." mode="render-points" />
</xsl:template>

<!-- mode: render-points -->

<xsl:template mode="render-points" match="node()|@*" />
<xsl:template mode="render-points" match="points">
	<xsl:apply-templates mode="render-points">
		<xsl:sort select="from/resource/dates/*[1]/@year" data-type="number" />
	</xsl:apply-templates>
</xsl:template>

<xsl:template mode="render-points" match="point">
	<xsl:apply-templates select="." mode="render-point" />
</xsl:template>
#+END_SRC
