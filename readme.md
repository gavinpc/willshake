# project willshake

Project willshake is an ongoing effort to bring the beauty and pleasure of
Shakespeare to new media.

Its home address is https://willshake.net

Please use the
[issue tracker](https://bitbucket.org/gavinpc/willshake/issues?status=new&amp;status=open)
to report problems.

For everything else, [contact@willshake.net](mailto:contact@willshake.net).
